const driverLocation = require('../src/models/driverLocations');
const drivers = require('../src/models/AuthN');
const pusher = require('../src/util/pusher');
const firebase = require('firebase-admin');
const cron = require('cron').CronJob;
const moment = require('moment');
module.exports = () => {
         new cron(new Date(moment().endOf('day').subtract(30, 'minutes')), async () => {
                  let onDrivers = await driverLocation.find({ status: 'on' }, { driverId: 1 }).lean(true);
                  for (let i = 0; i < onDrivers.length; i++) {
                           let updateStatus = await driverLocation.findOneAndUpdate({ driverId: onDrivers[i].driverId }, { status: 'off' }, { new: true });
                           if (updateStatus) {
                                    let driverToken = await drivers.findById({ _id: updateStatus.driverId }, { deviceToken: 1 }).lean(true);
                                    if (driverToken) {

                                             pusher.trigger(updateStatus.driverId, "updateDriverStatus", {
                                                      driverId: driverToken._id,
                                                      status: 'off'

                                             });
                                             const message = {
                                                      notification: {
                                                               title: 'You are offline',
                                                               body: 'Today time is up. Now you can manually on your status'
                                                      },
                                                      data: {
                                                               data: JSON.stringify({
                                                                        driverId: driverToken._id,
                                                                        status: 'off',
                                                                        actionType: "offline"

                                                               })
                                                      },
                                                      token: driverToken.deviceToken
                                             };
                                             await firebase.messaging().send(message).then(send =>
                                                      console.log('message send'));
                                    }
                           }
                  }
         }, null, true, 'Asia/Karachi')
}