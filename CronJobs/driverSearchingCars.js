const driverLocations = require('../src/models/driverLocations');
const cron = require('cron').CronJob;
const pusher = require('../src/util/adminPusher');
module.exports = () => {
         new cron('*/5 * * * *', async () => {
                  let busyCars = await driverLocations.find({ status: 'on' }, { location: 1 }).lean(true);
                  pusher.trigger('adminMap', 'driverSearchingCars', { busyCars })
         }, null, true, "Asia/Karachi");
}