const driverLocations = require('../src/models/driverLocations');
const rideOrders = require('../src/models/rider/rideOrders');
const cron = require('cron').CronJob;
const pusher = require('../src/util/adminPusher');
module.exports = () => {
         new cron('*/2  * * * *', async () => {
                  let totalCars = await driverLocations.find({}).countDocuments();
                  let offlineCars = await driverLocations.find({ status: 'off' }).countDocuments();
                  let orderAccepted = await rideOrders.find({ rideStatus: 'driverAccepted', orderStatus: 'pending' }).countDocuments();
                  let driverSearching = await rideOrders.find({ orderStatus: 'pending', rideStatus: 'driverSearching' }).countDocuments();
                  let waiting = await driverLocations.find({ status: 'on' }).countDocuments();
                  let onJob = await rideOrders.find({ rideStatus: 'rideStarted', orderStatus: 'pending' }).countDocuments();
                  pusher.trigger('adminMap', 'carscounter', { totalCars, offlineCars, orderAccepted, driverSearching, waiting, onJob })
         }, null, true, "Asia/Karachi");
}