const axios = require('axios');
const geocoder = require('../config/node-geocoder');
export const showPasses= async (req,res,next)=>{

    
    try {
        let geoData= await geocoder.reverse({lat:req.body.lat, lon:req.body.long});
    let passes = await axios.post('http://13.251.166.77:2000/api/v1/master/getAllPasses');
    let countryCode = geoData[0].countryCode;
    let city= geoData[0].city;
    let passesData = passes.data.response;
    let customerPasses =[];
    passesData.forEach(element => {
        if(element.country==countryCode&&element.city==city){
            customerPasses.push(element)
        }
    });
    res.status=200;
    res.json({code:'200',passes:customerPasses})
        
    } catch (error) {
        next(error);
    }
   
        


}