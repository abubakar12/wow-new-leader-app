const DriverOrderResponse = require('../models/driverOrderResponse');
const UserOrderRequest = require('../models/rider/userOrderRequestModel');
const UserLocation = require('../models/rider/userLocationModel');
const User = require('../models/rider/userModel');
const Driversignup = require('../models/AuthN');
const DriverLocation = require('../models/driverLocations');

const acceptOrRejectOrder = (req, res) => {
	let driverId = req.body.driverId;
	let orderId = req.body.orderId;
	let status = req.body.status;
	let rejectTime = undefined;
	if (status == 'accepted') {
		acceptTime = Date.now();
	}
	if (status == 'rejected') {
		rejectTime = Date.now();
	}
	DriverOrderResponse.findOneAndUpdate({
			orderId: orderId,
			driverId: driverId
		}, {
			status: status,
			acceptTime: acceptTime,
			rejectTime: rejectTime
		}, {
			new: true
		},
		(err, newDriverOrderResponse) => {
			if (err) {
				return res.json({
					code: '400',
					response: {
						msg: 'An error occured'
					}
				});
			} else if (!newDriverOrderResponse) {
				return res.json({
					code: '400',
					response: {
						msg: 'Order status Updation Failed'
					}
				});
			} else {
				return res.json({
					response: newDriverOrderResponse
				});
			}
		}
	);
};

const getDriverOrderResponseStatus = (req, res) => {
	let driverId = req.body.driverId;
	UserOrderRequest.findOne({
		driverId: driverId
	}).sort({
		_id: -1
	}).exec((err, userOrderRequest) => {
		if (err) {
			return res.json({
				code: '400',
				response: {
					msg: ' Order Response Status Failed'
				}
			});
		} else if (!userOrderRequest) {
			return res.json({
				code: '400',
				response: {
					msg: 'No Order Response Found'
				}
			});
		} else {
			if (
				userOrderRequest.status == 'accepted' ||
				userOrderRequest.status == 'driver arrived' ||
				userOrderRequest.status == 'start ride' ||
				userOrderRequest.status == 'completed'
				// ||
				// userOrderRequest.status == 'chalo'
			) {
				User.findOne({
					_id: userOrderRequest.userId
				}, (err, riderInfo) => {
					if (err) {
						return res.json({
							code: '400',
							response: {
								message: 'Rider Status failed'
							}
						});
					} else if (!riderInfo) {
						return res.json({
							code: '400',
							response: {
								message: 'Rider Status Updation Failed'
							}
						});
					} else {
						UserLocation.findOne({
							userId: userOrderRequest.userId
						}, (err, currentUserLocation) => {
							if (err) {
								return res.json({
									code: '400',
									response: {
										msg: 'Order status Updation Failed'
									}
								});
							} else if (!currentUserLocation) {
								return res.json({
									code: '400',
									response: {
										msg: 'Order status Updation Failed'
									}
								});
							} else {
								var driverSuccessResponse = new Object();
								driverSuccessResponse.userId = userOrderRequest.userId;
								driverSuccessResponse.riderName = riderInfo.fullName;
								driverSuccessResponse.riderPhoneNumber = riderInfo.fullPhoneNumber;
								driverSuccessResponse.riderRating = riderInfo.rating.toString();
								driverSuccessResponse.riderBatteryStatus = currentUserLocation.batteryStatus.toString();
								driverSuccessResponse.orderId = userOrderRequest._id;
								driverSuccessResponse.driverId = userOrderRequest.driverId;
								driverSuccessResponse.currency = userOrderRequest.currency;
								driverSuccessResponse.timeLabel = userOrderRequest.timeLabel;
								driverSuccessResponse.timeInSeconds = userOrderRequest.timeInSeconds.toString();
								driverSuccessResponse.distanceInKmLabel = userOrderRequest.distanceInKmLabel;
								driverSuccessResponse.distanceInMeters = userOrderRequest.distanceInMeters.toString();
								driverSuccessResponse.estimatedFareRange = userOrderRequest.estimatedFareRange.toString();
								driverSuccessResponse.pickupLocation = [
									userOrderRequest.pickupLocation.coordinates[0],
									userOrderRequest.pickupLocation.coordinates[1]
								];
								driverSuccessResponse.destinationLocation = [
									userOrderRequest.destinationLocation.coordinates[0],
									userOrderRequest.destinationLocation.coordinates[1]
								];
								driverSuccessResponse.driverLocation = [
									currentUserLocation.location.coordinates[0],
									currentUserLocation.location.coordinates[1]
								];

								driverSuccessResponse.status = userOrderRequest.status;
								if (userOrderRequest.status == 'accepted') {
									driverSuccessResponse.actionType = 'accepted';
								} else if (userOrderRequest.status == 'driver arrived') {
									driverSuccessResponse.actionType = 'rider-meetup';
								} else {
									driverSuccessResponse.actionType = 'start-ride';
								}
								if (
									userOrderRequest.status == 'completed' &&
									userOrderRequest.paymentStatus == 'completed'
								) {
									return res.json({
										code: '400',
										response: {
											msg: 'User Order request rejected'
										}
									});
								} else if (
									userOrderRequest.status == 'completed' &&
									userOrderRequest.paymentStatus == 'pending'
								) {
									driverSuccessResponse.actionType = 'completed';
									driverSuccessResponse.paymentStatus = 'pending';
								} else {}
								return res.json({
									code: '200',
									response: driverSuccessResponse
								});
							}
						});
					}
				});
			} else {
				return res.json({
					code: '400',
					response: {
						msg: 'User Order Request Rejected'
					}
				});
			}
		}
	});
};
module.exports = {
	acceptOrRejectOrder,
	getDriverOrderResponseStatus
};