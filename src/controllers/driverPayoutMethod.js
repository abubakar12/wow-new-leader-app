const payoutMethodClass = require('../Services/LeaderPayoutMethods');
const payoutMethod = new payoutMethodClass();

export const addNewPayoutMethod = async (req, res, next) => {
    try {
        let params = {
            driverId: req.body.driverId,
            payoutType: req.body.payoutType,
            address: req.body.address,
            accountHolderName: req.body.accountHolderName,
            bankAccountNumber: req.body.bankAccountNumber,
            IBAN: req.body.IBAN,
            bankName: req.body.bankName

        };
        
            await payoutMethod.addNewPayoutMethods(params);
            res.status(200).json({ code: '200', response: { msg: 'added successfully' } });
        

    } catch (err) {
        next(err);
    }
}

export const updatePayment = async (req, res, next) => {
    try {
        let params = {
            driverId: req.body.driverId,
            payoutType: req.body.payoutType,
            address: req.body.address,
            accountHolderName: req.body.accountHolderName,
            bankAccountNumber: req.body.bankAccountNumber,
            IBAN: req.body.IBAN,
            bankName: req.body.bankName

        };
            await payoutMethod.updatePayoutMethod(params);
            res.status(200).json({ code: '200', response: { msg: 'added successfully' } });
        

    } catch (err) {
        next(err);
    }
}

export const getPayoutMethods = async (req, res, next) => {
    try {
        let methods = await payoutMethod.getPayoutMethods();
        return res.json({ code: '200', response: methods });
    } catch (error) {
        next(error);
    }
}

export const getLeaderPayoutMethodById = async (req, res, next) => {
    try {
        let method = await payoutMethod.getLeaderPayout(req.body.driverId);
        res.status(200).json({ code: '200', response: method });
    } catch (error) {
        next(error);
    }
}

