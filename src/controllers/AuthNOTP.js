const Driver = require('../models/AuthN');
const pendingLeaders = require('../models/pendingLeaders');
const signUpTemp = require('../models/AuthNTemp');
const vehiclefiles_temp = require('../models/AuthNVehicleTemp');
const driverfiles_temp = require('../models/AuthNDriverTemp');

////// ****** Verify-API ****** //////
const otp_verification_start = (req, res) => {
	let fullPhoneNumber = req.body.countryCode + req.body.phoneNumber;
	let response = new Object();
	response.fullPhoneNumber = fullPhoneNumber; return res.json({ code: '200', response: response });
};
////// ****** Is-Verified-API ****** //////
const otp_verification_check = async (req, res) => {
	try {
		let response = new Object();
		const driver = await Driver.findOne({ fullPhoneNumber: req.body.fullPhoneNumber })
		if (!driver) {
			const pending = await pendingLeaders.findOne({ fullPhoneNumber: req.body.fullPhoneNumber })
			if (!pending) {
				const tempDriver = await signUpTemp.findOne({ fullPhoneNumber: req.body.fullPhoneNumber })
				if (!tempDriver) { response.userExists = 'notExist'; return res.json({ code: '200', response: response }) }
				else {
					let docId = tempDriver._id;
					console.log(docId);
					const driverDoc = await driverfiles_temp.findOne({ driverId: docId })
					const vehicleDoc = await vehiclefiles_temp.findOne({ driverId: docId })
					if (!driverDoc && !vehicleDoc) { return res.json({ code: '400', response: { msg: 'An error Occured' } }) }
					else {
						return res.json({
							code: '200', response: {
								userExists: 'temp',
								tempInfo: {
									_id: tempDriver._id,
									gender: tempDriver.gender,
									nationality:tempDriver.nationality,
									first_name: tempDriver.first_name,
									last_name: tempDriver.last_name,
									email: tempDriver.email,
									licenseNumber:tempDriver.licenseNumber,
									fullPhoneNumber: tempDriver.fullPhoneNumber,
									city: tempDriver.city,
									emiratesID: tempDriver.emiratesID,
									invite_code: tempDriver.invite_code,
									driver_photo: driverDoc.driver_photo,
									emirates_card_f: driverDoc.emirates_card_f,
									emirates_card_b: driverDoc.emirates_card_b,
									expiry_date_emirates: driverDoc.expiry_date_emirates,
									rta_card_f: driverDoc.rta_card_f,
									rta_card_b: driverDoc.rta_card_b,
									expiry_date_rta: driverDoc.expiry_date_rta,
									vehicle_reg_f: vehicleDoc.vehicle_reg_f,
									vehicle_reg_b: vehicleDoc.vehicle_reg_b,
									expiryDate: vehicleDoc.expiryDate,
									categoryId: vehicleDoc.categoryId,
									categoryName: vehicleDoc.categoryName,
									vehicleName: vehicleDoc.vehicleName,
									vehicleId: vehicleDoc.vehicleId,
									registrationNo: vehicleDoc.registrationNo,
									color: vehicleDoc.color,
									year: vehicleDoc.year,
									countryId:tempDriver.countryId,
									cityId:tempDriver.cityId
								}
							}
						})
					}
				}
			} else { response.userExists = 'pending'; return res.json({ code: '200', response: response }) }
		} else { response.userExists = 'exist'; return res.json({ code: '200', response: response }) }
	} catch (err) { return res.json({ code: "400", response: { msg: `${err.name} In otp_verification_check API ${err.message}` } }) }
}
////// ****** Verify-Status-API ****** //////
const otp_verification_status = (req, res) => {
	let requiredParams = ['phoneNumber', 'countryCode'];
	let errors = [];
	requiredParams.forEach((value) => { if (!req.body[value]) { errors.push(`${value} is required`) } });
	if (errors.length) { return res.status(200).json({ response: { code: '400', msg: errors[0] } }) }
	authy.phones().verification_status(req.body.phoneNumber, req.body.countryCode, function (error, response) {
		if (error) { return res.status(200).json({ response: { code: '400', msg: 'An error occured' } }) }
		else { return res.json({ response: response }) }
	});
};

module.exports = { otp_verification_start, otp_verification_check, otp_verification_status };