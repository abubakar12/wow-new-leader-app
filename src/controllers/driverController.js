const diverLocations = require('../Services/driver');
const location = new diverLocations();
const pusher = require('../util/pusher');
const request = require('request');
const { TWILIO_NUMBER } = require('../Constants/Twilio');
const firebase = require("firebase-admin");
const firebaseCreds = require('../config/firebaseCredentials.json');
//var sqs = require('../util/sqs');
const allCredential = require('../config/allCredential');
const AWS = require('aws-sdk');
const snsUrl = 'arn:aws:sns:ap-southeast-1:916147755851:driverLocation'
const cron = require('cron').CronJob;
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(allCredential.twilioSendGridApi, allCredential.twilioKeyId);
const twilio = require('twilio')(allCredential.twilioApiKey, allCredential.authToken);
AWS.config.update({
    accessKeyId: allCredential.accessKeyId,
    secretAccessKey: allCredential.secretAccessKey,
    region: 'ap-southeast-1'
});
firebase.initializeApp({
    credential: firebase.credential.cert(firebaseCreds),
    databaseURL: "https://wow-app-5f20f.firebaseio.com"
});
//Locations Modules 
const updateDriverLocation = async (req, res, next) => {
    try {

        let driverId = req.body.driverId;
        let batteryStatus = req.body.batteryStatus;
        let newLongitude = parseFloat(req.body.longitude);
        let newLatitude = parseFloat(req.body.latitude);
        let response = await location.updateLocation(driverId, batteryStatus, newLatitude, newLongitude);
        if (response) {

            if (response.status == 'on') {
                let longitude = response.location.coordinates[0];
                let latitude = response.location.coordinates[1];
                request.post('http://18.138.89.166:9000/api/v1/rider/customerStatus/findNearByCustomers', {
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        longitude,
                        latitude
                    })
                }, (error, resp) => {
                    if (error) {

                        next(error);
                    }

                    let customerId = JSON.parse(resp.body);
                    let ids = customerId.response.map(e => e.customerId);
                    pusher.trigger(ids, 'driverLocation', {
                        driverId: response.driverId,
                        driverBatteryStatus: response.batteryStatus,
                        longitude: response.location.coordinates[0].toString(),
                        latitude: response.location.coordinates[1].toString()
                    });
                });
                return res.json({ code: '200', response: { msg: 'successfully send pusher' } });
            }
            else if (response.status == 'engaged') {
                var params = {
                    Message: JSON.stringify({
                        driverId: response.driverId,
                        batteryStatus: response.batteryStatus,
                        location: response.location
                    }),
                    TopicArn: snsUrl
                };
                var publishTextPromise = new AWS.SNS({ apiVersion: '2010-03-31' }).publish(params).promise();
                publishTextPromise.then(
                    function (data) {
                        return res.json({ code: '200', response: { msg: 'successfully send sns' } });
                    }).catch(
                        function (err) {
                            next(err)
                        });
            }
            else if (response.status == 'off' || response.status == 'busy') {
                res.statusCode = 200;
                return res.json({ code: '200', response: { msg: 'Driver status is off/busy' } });
            }
            // let location={
            //     driverId:response.driverId,
            //     batteryStatus:response.batteryStatus,
            //     location:response.location
            // }
            // var params = {
            //     MessageBody: JSON.stringify(location),
            //     QueueUrl:queueUrl,
            //     MessageGroupId:"driverLocation"

            //   };
            //  sqs.sendMessage(params, function(err, data) {
            //     if (err){
            //         console.log("error", err.message);}
            //     else{
            //         console.log("Data",data);
            //     } 
            //   });  
            // res.statusCode = 200;
            // res.json({code:'200', response});
        }
        else if (!response) {
            res.statusCode = 400
            res.json({ code: '400', msg: 'error occured' });
        }
    }
    catch (error) {
        next(error);
    }

}
const findDriverByLocation = async (req, res) => {
    try {
        let drivers = await location.findNearDriversByLocation(req.body.latitude, req.body.longitude);
        if (drivers) {
            res.json({ code: '200', drivers })
        }
        else if (!drivers) {
            res.json({ code: '404', response: { msg: 'driver by location not found' } });
        }
    }
    catch (err) {
        res.json({ code: '400', response: { msg: 'an error occured', err } });
    }

}
const findDriverByStatus = async (req, res) => {
    try {
        let drivers = await location.findDriversByStatus(req.body.status, req.body.latitude, req.body.longitude);
        if (drivers) {
            res.json({ code: '200', drivers })
        }
        else if (drivers.errors) {
            res.json({ code: '400', response: { msg: 'an error occured', errors } });
        }
    }
    catch (err) {
        res.json({ code: '400', err });
    }
}
const findDriverByRadius = async (req, res) => {
    try {
        let drivers = await location.findDriverByRadius(req.body.status, req.body.radius, req.body.latitude, req.body.longitude);
        if (drivers) {
            res.json({ code: '200', drivers })
        }
        else if (drivers.errors) {
            res.json({ code: '400', response: { msg: 'an error occured', errors } });
        }
    }
    catch (err) {
        res.json({ code: '400', err });
    }
}
const findDriverId = async (req, res) => {
    try {
        let drivers = await location.findDriverByRadiusIds(req.body.status, req.body.radius, req.body.latitude, req.body.longitude);
        if (drivers) {
            res.statusCode = 200;
            res.json({ code: '200', drivers });
        }
        else if (drivers.errors) {
            res.json({ code: '400', response: { msg: 'an error occured', errors } });
        }
    }
    catch (err) {
        res.json({ code: '400', err });
    }
}
const countDriverByRadius = async (req, res) => {
    try {
        let drivers = await location.countDriverByRadius(req.body.status, req.body.radius, req.body.latitude, req.body.longitude);
        if (drivers || drivers == 0) {
            res.json({ code: '200', drivers })
        }
        else if (drivers.errors) {
            res.json({ code: '400', response: { msg: 'an error occured', errors } });
        }
    }
    catch (err) {
        res.json({ code: '400', err });
    }
}
//Ride Modules
const updateOrderStatus = async (req, res, next) => {
    try {
        let newTime = new Date();
        let Time = await location.getOrdersaveTime(req.body.driverId, req.body.orderId);
        let neww = new Date(Time.orderSaveTime)
        let timeDifference = Math.floor((newTime.getTime() - neww.getTime()) / 1000);
        console.log(timeDifference)
        if (timeDifference <= 13) {
            if (req.body.status == 'accepted') {
                let updateStatus = await location.changeOrderStatusAccept(req.body.driverId, req.body.orderId);
                if (updateStatus) {
                    res.statusCode = 200;
                    res.json({ code: '200', status: updateStatus.status });
                }
                else if (!updateStatus) {
                    res.statusCode = 404;
                    res.json({ response: { msg: 'can not update order' } });
                }
            }
            else if (req.body.status == 'rejected') {
                let updateStatus = await location.changeOrderStatusReject(req.body.driverId, req.body.orderId);
                if (updateStatus) {
                    res.statusCode = 200;
                    res.json({ code: '200', status: updateStatus.status });
                }
                else {
                    res.statusCode = 404;
                    res.json({ response: { msg: 'can not update order' } });
                }
            }
            else {
                res.statusCode = 400;
                res.json({ code: '400', response: { msg: 'please accept or reject the ride' } });
            }
        }
        else {
            res.statusCode = 400
            res.json({ code: '400', response: { msg: 'You run out of time' } });
        }


    }
    catch (err) {
        next(err)
    }

}
const startRide = async (req, res, next) => {
    try {
        let orderId = req.body.orderId;
        request.post("http://18.138.89.166:9003/api/v1/rider/rideorder/startRide", {
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                orderId
            })
        }, (error, response, body) => {
            if (error) {

                res.statusCode = 400
                res.json({ code: '400', response: { msg: 'an error occured during calling start ride api' } });
            }
            else if (response.statusCode == 200) {
                let location = JSON.parse(body)
                res.statusCode = 200
                res.json({ code: '200', response: { pickupLocation: location.response.pickupLocation, destinationLocation: location.response.destinationLocation } })
            }
            else if (response.statusCode == 400) {
                res.statusCode = 400
                res.json({ code: '400', response: { msg: 'Driver ride has not started successfully' } });
            }

        })

    } catch (error) {
        next(error);
    }

}
const driverArrivals = async (req, res, next) => {
    try {
        let orderId = req.body.orderId;
        request.post("http://18.138.89.166:9003/api/v1/rider/rideorder/driverArrival", {
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                orderId
            })
        }, (err, resp) => {
            if (err) {
                res.statusCode = 400
                res.json({ code: '400', response: { msg: 'an error occured during calling arrival api' } });
            }
            else if (resp.statusCode == 200) {
                res.statusCode = 200
                res.json({ code: '200', response: { msg: 'Driver arrived' } });
            }
            else if (resp.statusCode == 400) {
                res.statusCode = 400
                res.json({ code: '400', response: { msg: 'Failed to set driver arrival status' } });
            }
        });
    }
    catch (error) {
        next(error)
    }
}
const cancelRide = (req, res) => {
    try {
        let driverId = req.body.driverId;
        let orderId = req.body.orderId;
        let cancelReason = req.body.cancelReason;

        request.post('http://18.138.89.166:9003/api/v1/rider/rideorder/cancelRideByDriver', {
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                orderId,
                cancelReason
            })
        }, async (error, response) => {
            if (error) {
                res.statusCode = 400
                res.json({ code: '400', response: { msg: 'error occured in cancel api' } })
            }
            else if (response.statusCode == 200) {

                let rideCancel = JSON.parse(response.body);
                if (rideCancel.response.remainingMinutes >= 1) {
                    return res.json({ code: '200', response: { msg: `You can cancel ride in ${rideCancel.response.remainingMinutes} minutes` } });
                }
                else if (rideCancel.response.isCancelled == true) {
                    let updateStatus = await location.updateDriverStatusAfterCancel(driverId);
                    if (updateStatus) {
                        res.statusCode = 200
                        return res.json({ code: '200', response: { msg: 'ride canceled successfully' } });
                    }
                    else if (!updateStatus) {
                        res.statusCode = 400
                        return res.json({ code: '400', response: { msg: 'ride has not canceled' } });
                    }
                }


            }
            else if (response.statusCode == 400) {

                return res.status(400).json({ code: '400', response: { msg: `Can not cancel ride this time` } })

            }
        })

    } catch (error) {
        next(error);
    }
}
const updateDriverRating = async (req, res, next) => {
    try {
        let driverId = req.body.driverId;
        let rating = parseFloat(req.body.rating);
        let updateRate = await location.updateRating(driverId, rating);
        if (!updateRate) {
            res.statusCode = 404
            res.json({ code: '404', response: { msg: 'Driver not found' } });
        }
        res.statusCode = 200
        res.json({ code: '200', rating: updateRate })


    } catch (error) {
        next(error);
    }
};
const getRideStatus = async (req, res) => {
    let orders = await location.getRideStatus(req.body.driverId, orderId);
    res.json(orders);

}
const cancelOrder = async (req, res, next) => {
    try {
        let orderCancel = await location.cancelOrderByCustomer(req.body.driverId, req.body.orderId);
        if (orderCancel) {
            console.log("body", req.body);
            console.log("driver Id", req.body.driverId);
            console.log("actionType", req.body.actionType);

            let onStatus = await location.statusOn(req.body.driverId, 'on');
            pusher.trigger(req.body.driverId, req.body.actionType, req.body);
            let token = await location.getDToken(req.body.driverId);
            console.log("token", token.deviceToken)
            const message = {
                notification: {
                    title: 'Wow Leader',
                    body: 'Your order has cancelled'
                },
                data: {
                    data: JSON.stringify(req.body)
                },
                token: token.deviceToken
            };
            try {
                await firebase.messaging().send(message);
                console.log('successfully send FCM');
                res.statusCode = 200;
                return res.json({ code: '200' });
            } catch (e) {
                console.log('an error occured in FCM', e);
                res.statusCode = 200;
                return res.json({ code: '200' });
            }

        } else {
            res.statusCode = 400;
            res.json({ response: { msg: 'can not cancel the order' } });
        }
    }
    catch (error) {
        next(error);
    }
}


//Customer side driver ride Api
const updateOrderStatusCus = async (req, res) => {
    try {
        let updateStatus = await location.timePassedStatus(req.body.driverId, req.body.orderId);
        if (updateStatus) {
            res.statusCode = 200;
            res.json({ code: '200', updateStatus });
        }
        else {
            res.statusCode = 404;
            res.json({ code: '200', response: { msg: 'can not update order' } });
        }

    }
    catch (err) {
        res.statusCode = 400;
        res.json({ response: { msg: 'error in updating the request' }, err });
    }

}
const customerCancelRide = async (req, res) => {
    try {
        let cancel = await location.changeStatusCancelCus(req.body.driverId, req.body.orderId);
        if (cancel) {
            res.statusCode = 200
            return res.json({ code: '200', cancel });
        }
        else if (!cancel) {
            res.statusCode = 404
            return res.json({ code: '404', response: { msg: 'does not update the status' } });
        }
    }
    catch (err) {
        res.statusCode = 400
        return res.json({ code: '400', response: { msg: 'an error occured' } });

    }
}
const saveDriverOrder = async (req, res,next) => {
    try {

        let getDriverLoc = await location.getDriverLocation(req.body.driverId);
        if (getDriverLoc) {
            let placeOrder = await location.driverOrderSave(req.body.driverId, req.body.orderId, getDriverLoc.batteryStatus, getDriverLoc.location.coordinates[1], getDriverLoc.location.coordinates[0], req.body.requestSentTime);
            if (placeOrder) {
                let token = await location.getDToken(req.body.driverId);
                pusher.trigger(req.body.driverId, req.body.actionType, req.body);
                const message = {
                    notification: {
                        title: 'Ride Request',
                        body: 'You have new ride request'
                    },
                    data: {
                        data: JSON.stringify(req.body)
                    },
                    token: token.deviceToken
                };
                try {
                    await firebase.messaging().send(message);
                    console.log('successfully send FCM');
                    res.statusCode = 200;
                    return res.json({ code: '200' });
                } catch (e) {
                    console.log('an error occured in FCM', e);
                    res.statusCode = 200;
                    return res.json({ code: '200' });
                }
            }
        }
        else {
            res.statusCode = 400;
            res.json({ response: { msg: 'can not place the order' } });
        }

    }
    catch (err) {
        // console.log(err);
        next(err);
    }
}
const showorderStatus = async (req, res) => {
    try {
        let statuss = await location.showStatus(req.body.driverId, req.body.orderId);
        if (statuss) {
            if (statuss == 'accepted') {
                let driver = await location.driverBasicInfo(req.body.driverId);
                if (driver) {
                    res.statusCode = 200;
                    return res.json({ code: '200', status: statuss, driver });
                }
                else if (!driver) {
                    res.statusCode = 404;
                    res.json({ response: { msg: 'can not get driver detail' } });
                }
            }
            else if (statuss == 'rejected' || statuss == 'timePassed' || statuss == 'pending') {
                res.statusCode = 200;
                return res.json({ code: '200', status: statuss });

            }
        }

        else if (!driver) {
            res.statusCode = 404;
            res.json({ response: { msg: 'can not show order' } });
        }
    }
    catch (err) {

        res.statusCode = 400;
        res.json({ response: { msg: 'error in showing the request' }, err });
    }
}

//Wallet Side Module
const rideWeekHistory = async (req, res) => {
    try {

        let response = await location.rideHistoryWeekly(req.body.driverId, req.body.startDate, req.body.endDate);

        if (response) {
            res.statusCode = 200;
            res.json({ code: '200', response });
        }
    }
    catch (err) {

        res.statusCode = 404
        res.json({ code: '400', response: { msg: "can not get " } });
    }
}
const ridesHistory = async (req, res) => {
    try {
        let rides = [];
        let ride = await location.rideHistory(req.body.driverId, req.body.startDate, req.body.endDate);
        if (ride != '') {
            ride.forEach(item => {
                rides.push({
                    customerId: item.customerId,
                    date: item.date,
                    orderRating: item.orderRating,
                    amount: `AED ${item.amount}`,
                    distance: `${item.distance} Km`,
                    pickLocation: item.pickLocation,
                    pickTime: item.pickTime,
                    dropLocation: item.dropLocation,
                    dropTime: item.dropTime
                });

            })
            res.json({ code: '200', ride })
        }
        else if (rides == '') {

            res.json({ code: '404', response: { msg: 'you have no orders' } });
        }
    }

    catch (err) {
        // let err= JSON.stringify(error)
        res.json({ code: '400', response: { msg: 'an error occured showing the rides history', error: err.message } });
    }
}

const driverWallet = async (req, res, next) => {
    try {
        let getCountryCode = await location.getDriverDetail(req.body.driverId);
        let code = getCountryCode.countryCode;
        var countryCode = code.substr(1);
        var d = await location.getCodeCountry(countryCode);

        let params = {
            driverId: req.body.driverId,
            wallet: req.body.wallet,
            outstandingAmount: req.body.outstandingAmount,
            currency: d.currency,
            countryId:getCountryCode.countryId,
            cityId:getCountryCode.cityId
        }

        let wallet = await location.driverWallet(params);
        if (wallet) {
            res.statusCode = 200
            res.json({ code: '200', wallet });
        }
        else if (!wallet) {
            res.statusCode = 400
            res.json({ code: '400', response: { msg: 'could not add wallet' } });
        }
    }
    catch (error) {

        next(error);
    }

}
const getDriverWallet = async (req, res, next) => {
    try {
        let wallet = await location.getWallet(req.body.driverId);
        if (wallet) {
            res.statusCode = 200
            res.json({ code: '200', response: wallet })
        }
    } catch (error) {
        next(error);
    }
}
//Review Module 
const saveReviews = async (req, res) => {
    try {
        let review = await location.saveDriverReview(req.body.driverId, req.body.profile, req.body.name, req.body.rating, req.body.comments);
        if (review) {
            res.statusCode = 200
            res.json({ code: '200', review });
        }
    }
    catch (err) {
        res.statusCode = 400
        res.json({ code: '400', response: { msg: 'can not save review' } });
    }
}
const showReviews = async (req, res) => {
    try {
        let response = await location.showReview(req.body.driverId);
        if (response) {
            res.statusCode = 200
            res.json({ code: '200', response });
        }
        else if (!response) {
            res.statusCode = 404
            res.json({ code: '400', response: { msg: 'you have no review yet' } });
        }
    }
    catch (err) {
        res.statusCode = 400
        res.json({ code: '400', response: { msg: 'can not save review' } });
    }
}


//Driver Data related Modules
const getVehicleDetail = async (req, res) => {
    try {
        let response = await location.showVehicleDetails(req.body.driverId);
        if (response) {
            res.statusCode = 200
            res.json({ code: '200', response });
        }
        else if (!response) {
            res.statusCode = 400
            res.json({ code: '400', response: { msg: 'have no car' } });
        }

    } catch (err) {
        res.statusCode = 400
        res.json({ code: '400', response: { msg: 'error occured' } });

    }
}
const driverFiles = async (req, res) => {
    try {
        if (req.body.driverId != '', req.body.fileName == 'leaderDocument') {
            let leaderDocs = await location.driverDocuments(req.body.driverId);
            if (leaderDocs != '') {
                res.json({ code: '200', leaderDocs });

            }
            else {
                res.json({ code: '400', response: { msg: 'an error occured in retrieving the leader docs' } });
            }
        }
        else if (req.body.driverId != '', req.body.fileName == 'vehicleDocument') {
            let vehicleDocs = await location.vehicleDocuments(req.body.driverId);
            if (vehicleDocs != '') {
                res.json({ code: '200', vehicleDocs });

            }
            else {
                res.json({ code: '400', response: { msg: 'an error occured in retrieving the vehicleDocs' } });
            }
        }


    } catch (err) {
        res.json({ code: '400', response: { msg: 'an error occured in driver documents', err: err.stack } });
    }
}
const driverInfoBasic = async (req, res, next) => {
    try {
        let driverInfo = await location.driverBasicInfo(req.body.driverId);
        if (driverInfo) {
            res.statusCode = 200
            res.json({ driverInfo });
        }
        else {
            res.statusCode = 400
            res.json({ code: '400', response: { msg: "driver info not retrieve" } });
        }

    }
    catch (err) {
        next(err);
    }
}
const getDriverDeviceToken = async (req, res, next) => {
    try {
        let driverToken = await location.getDToken(req.body.driverId);
        if (driverToken) {
            res.statusCode = 200
            res.json({ code: '200', driverToken });
        }
        else {
            res.statusCode = 400
            res.json({ code: '400', response: { msg: ' can not get token' } });
        }

    }
    catch (error) {
        next(error);
    }
}
const getAllDriversOrCustomerContacts = async (req, res, next) => {

    try {
        let role = req.body.role;
        if (role == 'driver') {
            let contacts = await location.getDriverContacts();
            return res.status(200).json({ code: '200', contacts })
        }
        else if (role == 'customer') {
            request.get('http://18.138.89.166:9000/api/v1/rider/customer/getCustomersContacts', {}, (error, response, body) => {
                if (error) {
                    next(error);
                }
                let contacts = JSON.parse(body);
                return res.status(200).json({ code: '200', contacts: contacts.contacts })
            });
        }

    } catch (error) {
        next(error);
    }
}
const saveDriverPreferences = async (req, res, next) => {
    try {
        const { driverId, baggage, passenger, childSeat, rideAssistance, drinkingWater, temperature } = req.body;
        let preferences = await location.saveLeaderPreferences(driverId, baggage, passenger, childSeat, rideAssistance, drinkingWater, temperature);
        if (!preferences) {
            res.statusCode = 400
            res.json({ code: '400', response: { msg: 'driver not found' } })
        }
        res.statusCode = 200
        res.json({ code: '200', response: { msg: 'successfully updated' } });

    } catch (error) {
        next(error)
    }


}
const showDriverPreferences = async (req, res, next) => {
    try {
        let preferences = await location.showLeaderSelectedPreferences(req.body.driverId);
        
        if (preferences.preferences) {
            res.statusCode = 200
            res.json({ code: '200', preferences: preferences.preferences });
        }
        else if(!preferences.preferences) {
            res.statusCode=200
            res.json({code:'400',response:{msg:'No driver preference added yet!'}});
        }

    } catch (error) {
        next(error)
    }

}
const sendInvitationThroughSms = async (req, res, next) => {
    try {
       twilio.messages.create({
           body:"https://wowets.com/",
           from:TWILIO_NUMBER,
           to:req.body.number
       }).then(message=>{
        res.statusCode=200
        res.json({code:'200',response:{msg:`successfully send message to ${req.body.number}`}})}).catch(error=>{
            next(error)
        })

    } catch (error) {
        next(error)
    }
}

//temporary
const driverAcceptRide = async (req, res) => {
    try {
        let accept = await location.changeOrderStatusAccept(req.body.driverId, req.body.orderId);
        if (accept) {
            res.statusCode = 200
            return res.json({ code: '200', accept });
        }
        else if (!accept) {
            res.statusCode = 404
            return res.json({ code: '404', response: { msg: 'does not update the status' } });
        }
    }
    catch (err) {
        res.statusCode = 400
        return res.json({ code: '400', response: { msg: 'an error occured' } });

    }
}
const driverRejectRide = async (req, res, next) => {
    try {
        let reject = await location.changeOrderStatusReject(req.body.driverId, req.body.orderId);
        if (reject) {
            res.statusCode = 200
            return res.json({ code: '200', reject });
        }
        else if (!reject) {
            res.statusCode = 404
            return res.json({ code: '404', response: { msg: 'does not update the status' } });
        }
    }
    catch (err) {
        next(err)
        res.statusCode = 400
        return res.json({ code: '400', response: { msg: 'an error occured' } });


    }
}
const driverCancelRide = async (req, res) => {
    try {
        let accept = await location.changeOrderStatusCancel(req.body.driverId, req.body.orderId);
        if (accept) {
            res.statusCode = 200
            return res.json({ code: '200', accept });
        }
        else if (!accept) {
            res.statusCode = 404
            return res.json({ code: '404', response: { msg: 'does not cancel' } });
        }
    }
    catch (err) {
        res.statusCode = 400
        return res.json({ code: '400', response: { msg: 'an error occured' } });

    }
}
const statusOnDriver = async (req, res) => {
    try {
        let status = await location.statusOn(req.body.driverId, req.body.status);
        if (status) {
            return res.status(200).json({ code: '200', status: status.status });
        }


    } catch (error) {
        next(error);
    }
}
const inviteContactsSearchDriver = async (req, res) => {
    let contacts = await location.searchContactsForDriverInvite(req.body.contacts);
    if (contacts == '') {
        res.statusCode = 200
        return res.json({ code: '200', response: { msg: 'No contact for invite' } });
    }
    else if (contacts) {
        res.statusCode = 200
        return res.json({ code: '200', contacts });
    }
}



const test = async (req, res) => {
    // new cron('* * * * * *',()=>{

    //     pusher.trigger('test', 'my', {
    //         message: 'hello world'
    //       })
    // },null,true,'Asia/Karachi');



}
const getCitiesAirports = async (req, res, next) => {
    let city = req.body.city;
    try {
        request.get(' http://aviation-edge.com/v2/public/autocomplete', {
            qs: {
                key: "3f7871-376183",
                city: city
            }
        }, (error, resp, body) => {
            if (error) {
                next(error)
            }
            let airports = JSON.parse(body);
            res.json({ code: '200', airports: airports.airportsByCities })
        })
    } catch (error) {
        next(error);
    }

}


//Testing 
const calculateLoginTime = async (req, res) => {
    let online = await location.getOnlineTime(req.body.driverId);
    if (online) {
        res.json(online)
    }

}
const getAlldriverWallet = async (req, res, next) => {
    try {
        let wallet = await location.getAllDriverWallet(req.params.page, req.params.limit);
        if (wallet) {
            return res.json({ code: "200", response: wallet })
        }
        else if (!wallet) {
            return res.json({ code: '400', response: { msg: "no wallet found" } });
        }
    } catch (error) {
        next(error)
    }

}
const getServerTime = (req, res, next) => {
    try {
        return res.status(200).json({ code: '200', date: new Date() });
    } catch (error) {
        next(error)
    }
}
const getDriverTotalRidesAndTime = async (req, res, next) => {
    try {
        let driverId = req.body.driverId;
        let totalRides;
        let totalEarned;
        // let logs = await location.getTotalOnlineTime(driverId);


        request.get('http://18.138.89.166:9002/api/v1/transactionHistory/getLeaderTotalRidesAndEaring',
            {
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ driverId })
            }, (error, response, body) => {
                if (error) next(error);
                let data = JSON.parse(body);
                res.statusCode = 200
                return res.json({ code: '200', response: { totalRides: data.totalRides, totalEarned: data.totalEarned, totalOnlineTime: "00:00" } })

            });

    } catch (error) {
        next(error)
    }
}

const testPusher = (req, res,next) => {
    console.log(req.body);
    res.status(200).send('ok')

}
const sendEmailForHelp = async(req,res,next)=>{
    let email = await location.driverBasicInfo(req.body.driverId);
    const emailMsg = {
        to: `${req.body.email}`,
        from: `${email.email}`,
        subject:`${req.body.subject}`,
        html: `<p>${req.body.emailBody}</p>`,
    }
    sgMail.send(emailMsg).then(message=>{
       res.statusCode=200;
       return res.json({code:'200',response:{msg:"Email Sent"}});
    }).catch(error=>{
        next(error);
    });

}


module.exports = {
    findDriverByLocation, updateDriverLocation, findDriverByStatus, findDriverByRadius, ridesHistory, countDriverByRadius,
    findDriverId, driverFiles, saveDriverOrder, updateOrderStatus, showorderStatus, rideWeekHistory, test, saveReviews, showReviews,
    updateOrderStatusCus, driverAcceptRide, driverRejectRide, driverCancelRide, customerCancelRide, getVehicleDetail, startRide,
    driverInfoBasic, driverArrivals, getRideStatus, getDriverDeviceToken, cancelRide, statusOnDriver, updateDriverRating,
    calculateLoginTime, cancelOrder, inviteContactsSearchDriver, driverWallet, getDriverWallet, getCitiesAirports, getAlldriverWallet,
    getServerTime, getDriverTotalRidesAndTime,
    getAllDriversOrCustomerContacts, testPusher, saveDriverPreferences, showDriverPreferences, sendInvitationThroughSms,sendEmailForHelp
};