const DriverLocation = require('../models/driverLocations');
const turf = require('@turf/turf');
const statusLogs = require('../models/driverLoginsLogs');
const moment = require('moment')


// Update Driver Location 1st API EndPoint
const createUpdateDriverLocation = (req, res) => {
	let driverId = req.body.driverId;
	let batteryStatus = req.body.batteryStatus;
	let newLongitude = parseFloat(req.body.longitude);
	let newLatitude = parseFloat(req.body.latitude);

	DriverLocation.findOne(
		{
			driverId: req.body.driverId
		},
		(err, driverLocation) => {
			if (err) {
				return res.json({ code: '400', response: { msg: 'An error occured' } });
			} else if (driverLocation) {
				let prevLocationLon = driverLocation.prevLocation.coordinates[0];
				let prevLocationLat = driverLocation.prevLocation.coordinates[1];
				let from = turf.point([ prevLocationLon, prevLocationLat ]);
				let to = turf.point([ newLongitude, newLatitude ]);
				let prevLocation = {
					type: 'Point',
					coordinates: [ prevLocationLon, prevLocationLat ]
				};
				let distance = turf.distance(from, to);
				if (distance >= 0.005) {
					prevLocationLon = newLongitude;
					prevLocationLat = newLatitude;
					prevLocation.coordinates = [ prevLocationLon, prevLocationLat ];
				}
				DriverLocation.findOneAndUpdate(
					{ driverId: driverId },
					{
						batteryStatus: batteryStatus,
						prevLocation: prevLocation,
						'location.coordinates.0': newLongitude,
						'location.coordinates.1': newLatitude
					},
					{ new: true },
					(err, updatedDriverLocation) => {
						if (err) {
							return res.json({
								code: '400',
								response: { msg: 'DriverLocation updation failed' }
							});
						} else if (!updatedDriverLocation) {
							return res.json({
								code: '400',
								response: { msg: 'DriverLocation updation failed' }
							});
						} else {
							return res.json({ response: updatedDriverLocation });
						}
					}
				);
			} else {
				const newDriverLocation = new DriverLocation({
					driverId: driverId,
					batteryStatus: batteryStatus,
					prevLocation: {
						type: 'Point',
						coordinates: [ newLongitude, newLatitude ]
					},
					location: {
						type: 'Point',
						coordinates: [ newLongitude, newLatitude ]
					}
				});
				newDriverLocation.save((err, driverLocation) => {
					if (err) {
						return res.json({
							code: '400',
							response: { msg: 'DriverLocation creation failed' }
						});
					} else if (!driverLocation) {
						return res.json({
							code: '400',
							response: { msg: 'DriverLocation creation failed' }
						});
					} else {
						return res.json({ response: driverLocation });
					}
				});
			}
		}
	);
};

// Driver Modify Online and Offline Status 2ND API EndPoint

const changeDriverStatus = (req, res) => {
	let driverId = req.body.driverId;
	let status = req.body.status;
	DriverLocation.findOneAndUpdate(
		{ driverId: driverId },
		{ status: status },
		{ new: true },
		async (err, driverLocation) => {
			if (err) {
				return res.json({ code: '400', response: { msg: ' Driver Status Failed to Change' } });
			} else if (!driverLocation) {
				return res.json({ code: '400', response: { msg: 'Driver Status Failed' } });
			} else {
				if(driverLocation.status=='off'){
					let findLogs = await statusLogs.findOne({driverId,createdAt:{$gt:moment(new Date()).startOf('D'),$lt:moment(new Date()).endOf('D')}});
					if(!findLogs) console.log('no log found');
						findLogs.logs[findLogs.logs.length-1].offTime = new Date();
						let off = moment.duration(moment(findLogs.logs[findLogs.logs.length-1].offTime).diff(moment(findLogs.logs[findLogs.logs.length-1].onTime))).asMilliseconds();
						findLogs.logs[findLogs.logs.length-1].timeDifference = off;
						findLogs.save();}
						else if(driverLocation.status=='on'){
							
							let checkLog = await statusLogs.findOne({driverId,createdAt:{$gt:moment(new Date()).startOf('D'),$lt:moment(new Date()).endOf('D')}});
				if(!checkLog){
					let saveLog=new statusLogs({
						driverId:driverId,
						logs:{
							onTime:new Date()
						}
					});
					await saveLog.save();
				}
				else {
					
					if(checkLog.logs[checkLog.logs.length-1].offTime==null)
					{
						console.log('no update');
					}
					else {
						let checkLog = await statusLogs.updateOne({driverId,createdAt:{$gt:moment(new Date()).startOf('D'),$lt:moment(new Date()).endOf('D')}},{$push:{logs:{onTime:new Date()}}});

					}	
		}
						}
				return res.json({ code: '200', response: driverLocation.status });
			}
		}
	);
};

// Find near by drivers 3RD API EndPoint

const findNearByDrivers = (req, res) => {
	let driverId = req.body.driverId;
	let longitude = parseFloat(req.body.longitude);
	let latitude = parseFloat(req.body.latitude);
	const driverLocation = new DriverLocation({
		driverId: driverId,
		location: {
			type: 'Point',
			coordinates: [ longitude, latitude ]
		}
	});
	driverLocation.findNearByDrivers((err, drivers) => {
		if (err || !drivers) {
			return res.json({ code: '400', response: { msg: 'An error occured' } });
		} else if (drivers.length == 0) {
			return res.json({ response: { msg: 'No nearby drivers found' } });
		} else {
			drivers.forEach((driver) => {
				driver.createdAt = undefined;
				driver.updatedAt = undefined;
			});
			return res.json({ response: drivers });
		}
	});
};

module.exports = { createUpdateDriverLocation, changeDriverStatus, findNearByDrivers };
