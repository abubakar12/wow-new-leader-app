///////// *********** All Require Models ///////// ***********
const Driversignup = require('../models/AuthN');
const pendingLeaders = require('../models/pendingLeaders');
const allCredential = require('../config/allCredential');
const DriverLocation = require('../models/driverLocations');
const vehiclefiles_temp = require('../models/AuthNVehicleTemp');
const vehicleCards = require('../models/AuthNVehicle');
const driverfiles_temp = require('../models/AuthNDriverTemp');
const signUpTemp = require('../models/AuthNTemp');
const uniqueString = require('unique-string');
const driverCards = require('../models/AuthNDriver');
const rideHistory = require('../models/orderHistory');
const vehicleCategories = require('../models/vehicleCategory');
const request = require('request');
const statusLogs = require('../models/driverLoginsLogs');
const moment = require('moment')
const AWS = require('aws-sdk');
const rideOrder = require('../models/rider/rideOrders');
const driverFunctions = require('../Services/driver');
const driverService = require('../Services/driver');
const payoutMethods = require('../Services/LeaderPayoutMethods');
const methods = new payoutMethods();
const driverServices = new driverService();
const cron = require('cron').CronJob;
const pusher = require('../util/pusher')
const firebase = require('firebase-admin');
const payoutMethod = require('../models/LeaderPayoutMethods');
const firebaseCreds = require('../config/firebaseCredentials.json');
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(allCredential.twilioSendGridApi, allCredential.twilioKeyId);

AWS.config.update({ accessKeyId: allCredential.accessKeyId, secretAccessKey: allCredential.secretAccessKey, region: 'ap-southeast-1' });
var cloudBase = new AWS.S3({ params: { Bucket: 'wow-leader-app', ACL: 'public-read' } });
// For Random Pin Generate 
function randomIntInc(low, high) { return Math.floor(Math.random() * (high - low + 1) + low) }

function leftPad(str, length) {
	str = str == null ? '' : String(str)
	length = ~~length
	var pad = ''
	var padLength = length - str.length
	while (padLength--) { pad += '0' }
	return pad + str
}
// For Custom Date Validation


//////****** temporarySignUp API ******///////
const temporarySignUp = async (req, res) => {
	function decodeBase64Image(dataString) {
		let response = {};
		const base64Data = new Buffer.from(dataString.replace(/^data:image\/\w+;base64,/, ""), 'base64');
		const type = dataString.split(';')[0].split('/')[1];
		response.type = type;
		response.data = base64Data;
		return response;
	}
	try {
		var d_id = req.body.driverId;
		if (d_id == '') {
			const driverEmail = await Driversignup.find({ email: req.body.email }).countDocuments();
			const driverEmailPending = await pendingLeaders.find({ email: req.body.email }).countDocuments();
			if (driverEmail == 0 && driverEmailPending == 0) {
				if (req.body.fileName == 'signUpInfo') {
					const scards = new signUpTemp(req.body);
					const result = await scards.save();
					if (result) {
						const vcards = new vehiclefiles_temp({ driverId: result._id, vehicle_reg_f: '', vehicle_reg_b: '', expiryDate: '', categoryId: '', categoryName: '', vehicleName: '', vehicleId: '', registrationNo: '', color: '', year: '' });
						let vcardsSave = await vcards.save();
						if (vcardsSave) {
							const dcards = new driverfiles_temp({ driverId: result._id, driver_photo: '', emirates_card_f: '', emirates_card_b: '', expiry_date_emirates: '', rta_card_f: '', rta_card_b: '', expiry_date_rta: '', });
							let dCardSave = await dcards.save();
							if (dCardSave) {
								return res.json({ code: '200', response: { driverId: result._id } })
							}
						}
					}
				}
			}
			else {
				return res.json({ code: '400', response: { msg: 'Email already exist' } })
			}
		}
		let driverId = req.body.driverId
		if (req.body.fileName == 'signUpInfo') {
			const driverUpdation = await signUpTemp.findOneAndUpdate({ _id: driverId }, req.body, { new: true })
			return res.json({ code: '200', response: { driverId: driverUpdation._id } })
		}
	} catch (err) {
		console.log(err);
		return res.json({ code: "400", response: { msg: `${err.name} In temporarySignup API ${err.message}` } })
	}
	try {
		var driverId = req.body.driverId;
		if (req.body.fileName == 'vehicleInfo') {
			const category = await vehicleCategories.findOne({ _id: req.body.categoryId })
			const vehicles = category.vehicles;
			const vehicle = await vehicles.find(vehicle => vehicle.id == req.body.vehicleId);
			if (!category && !vehicle) { return res.json({ code: '400', response: { msg: 'Data is not inserted' } }) }
			const category_id = category._id;
			const category_name = category.categoryName;
			const doc = await vehiclefiles_temp.findOneAndUpdate({ driverId: driverId }, {
				categoryId: category_id, categoryName: category_name, vehicleId: vehicle.id, vehicleName: vehicle.name, registrationNo: req.body.registrationNo, year: req.body.year, color: req.body.color,
			}, { new: true })
			if (!doc) { return res.json({ code: '400', response: { msg: 'Data is not inserted' } }) }
			return res.json({ code: '200', response: { driverId: doc.driverId, categoryId: doc.categoryId, categoryName: doc.categoryName, vehicleId: doc.vehicleId, vehicleName: doc.vehicleName, registrationNo: doc.registrationNo, year: doc.year, color: doc.color } });
		} else if (req.body.fileName == 'vehicle_reg_f') {
			let vehicle_reg_f = req.body.fileData;
			let Img = decodeBase64Image(vehicle_reg_f);
			let fileRename = driverId + '_vehfront.jpg';
			const params = { Key: ('driverImages/' + fileRename), Body: Img.data, ContentEncoding: 'base64', requiredContentType: Img.type }
			const docs = await cloudBase.upload(params).promise();
			const doc = await vehiclefiles_temp.findOneAndUpdate({ driverId: driverId, }, { vehicle_reg_f: docs.Location, expiryDate: req.body.expiryDate }, { new: true })
			if (!doc) { return res.json({ code: '400', response: { msg: 'File Not uploaded!' } }); }
			return res.json({ code: '200', response: { driverId: doc.driverId, vehicle_reg_f: doc.vehicle_reg_f, expiryDate: doc.expiryDate } })
		} else if (req.body.fileName == 'vehicle_reg_b') {
			let vehicle_reg_b = req.body.fileData;
			let Img = decodeBase64Image(vehicle_reg_b);
			let fileRename = driverId + '_vehback.jpg';
			const params = { Key: ('driverImages/' + fileRename), Body: Img.data, ContentEncoding: 'base64', ContentType: Img.type }
			const docs = await cloudBase.upload(params).promise();
			const doc = await vehiclefiles_temp.findOneAndUpdate({ driverId: driverId }, { vehicle_reg_b: docs.Location }, { new: true })
			if (!doc) { return res.json({ code: '400', response: { msg: 'File Not uploaded!' } }) }
			return res.json({ code: '200', response: { driverId: doc.driverId, vehicle_reg_b: doc.vehicle_reg_b, } });
		}
	} catch (err) { return res.json({ code: "400", response: { msg: `${err.name} In temporarySignup API ${err.message}` } }) }
	try {
		var driverId = req.body.driverId;
		if (req.body.fileName == 'driver_photo') {
			let driver_photo = req.body.fileData;
			let Img = decodeBase64Image(driver_photo);
			let fileRename = driverId + '_drv_Photo.jpg';
			const params = { Key: ('driverImages/' + fileRename), Body: Img.data, ContentEncoding: 'base64', ContentType: Img.type }
			const docs = await cloudBase.upload(params).promise();
			const doc = await driverfiles_temp.findOneAndUpdate({ driverId: driverId }, { driver_photo: docs.Location }, { new: true })
			if (!doc) { return res.json({ code: '400', response: { msg: 'File Not uploaded!' } }) }
			return res.json({ code: '200', response: { driverId: doc.driverId, driver_photo: doc.driver_photo, } });
		} else if (req.body.fileName == 'emirates_card_f') {
			let emirates_card_f = req.body.fileData;
			let Img = decodeBase64Image(emirates_card_f);
			let fileRename = driverId + '_efront.jpg';
			const params = { Key: ('driverImages/' + fileRename), Body: Img.data, ContentEncoding: 'base64', ContentType: Img.type }
			const docs = await cloudBase.upload(params).promise();
			const doc = await driverfiles_temp.findOneAndUpdate({ driverId: driverId }, { emirates_card_f: docs.Location, }, { new: true })
			if (!doc) { return res.json({ response: { doc: doc, code: '400', msg: 'File Not uploaded!' } }) }
			return res.json({ code: '200', response: { driverId: doc.driverId, emirates_card_f: doc.emirates_card_f } });
		} else if (req.body.fileName == 'emirates_card_b') {
			let emirates_card_b = req.body.fileData;
			let Img = decodeBase64Image(emirates_card_b);
			let fileRename = driverId + '_eback.jpg';
			const params = { Key: ('driverImages/' + fileRename), Body: Img.data, ContentEncoding: 'base64', ContentType: Img.type }
			const docs = await cloudBase.upload(params).promise();
			const doc = await driverfiles_temp.findOneAndUpdate({ driverId: driverId }, { emirates_card_b: docs.Location, expiry_date_emirates: req.body.expiry_date_emirates }, { new: true })
			if (!doc) { return res.json({ response: { doc: doc, code: '400', msg: 'File Not uploaded!' } }) }
			return res.json({ code: '200', response: { driverId: doc.driverId, emirates_card_b: doc.emirates_card_b, expiry_date_emirates: doc.expiry_date_emirates } });
		} else if (req.body.fileName == 'rta_card_f') {
			let rta_card_f = req.body.fileData;
			let Img = decodeBase64Image(rta_card_f);
			let fileRename = driverId + '_rtafront.jpg';
			const params = { Key: ('driverImages/' + fileRename), Body: Img.data, ContentEncoding: 'base64', ContentType: Img.type }
			const docs = await cloudBase.upload(params).promise();
			const doc = await driverfiles_temp.findOneAndUpdate({ driverId: driverId }, { rta_card_f: docs.Location, }, { new: true })
			if (!doc) { return res.json({ response: { doc: doc, code: '400', msg: 'File Not uploaded!' } }); }
			return res.json({ code: '200', response: { driverId: doc.driverId, rta_card_f: doc.rta_card_f, } });
		} else if (req.body.fileName == 'rta_card_b') {
			let rta_card_b = req.body.fileData;
			let expiry_date_rta = req.body.expiry_date_rta
			let Img = decodeBase64Image(rta_card_b);
			let fileRename = driverId + '_rtaback.jpg';
			const params = { Key: ('driverImages/' + fileRename), Body: Img.data, ContentEncoding: 'base64', ContentType: Img.type }
			const docs = await cloudBase.upload(params).promise();
			const doc = await driverfiles_temp.findOneAndUpdate({ driverId: driverId }, { rta_card_b: docs.Location, expiry_date_rta: expiry_date_rta }, { new: true })
			if (!doc) { res.json({ response: { doc: doc, code: '400', msg: 'File Not uploaded!' } }) }
			return res.json({ code: '200', response: { driverId: doc.driverId, rta_card_b: doc.rta_card_b, expiry_date_rta: doc.expiry_date_rta, } });
		}
		else if (req.body.fileName == 'payoutMethod') {
			let params = {
				driverId: req.body.driverId,
				payoutType: req.body.payoutType,
				address: req.body.address,
				accountHolderName: req.body.accountHolderName,
				bankAccountNumber: req.body.bankAccountNumber,
				IBAN: req.body.IBAN,
				bankName: req.body.bankName
			};
			let saveMethod = await methods.addNewPayoutMethods(params);
			if (saveMethod) { return res.status(200).json({ code: '200', response: saveMethod }) }
		}
	} catch (err) { return res.json({ code: "400", response: { msg: `${err.name} In temporarySignup API ${err.message}` } }) }
}
//////****** PendingSignUp API ******///////
const pendingSignUp = async (req, res) => {
	try {
		var pin = leftPad(randomIntInc(1000, 9999), 4)
		if (!req.body.driverId) { return res.json({ code: '400', response: { msg: 'driverId is required' } }) }
		const driverId = req.body.driverId;
		var driver = await signUpTemp.findByIdAndRemove({ _id: driverId })
		const signup = new pendingLeaders({ first_name: driver.first_name, last_name: driver.last_name, email: driver.email, countryCode: driver.countryCode, phoneNumber: driver.phoneNumber, fullPhoneNumber: driver.fullPhoneNumber, city: driver.city, emiratesID: driver.emiratesID, gender: driver.gender, pinCode: pin, nationality: driver.nationality, licenseNumber: driver.licenseNumber, countryId: driver.countryId, cityId: driver.cityId });
		const data = await signup.save();
		await vehiclefiles_temp.findOneAndUpdate({ driverId: driver._id }, { driverId: data._id }, { new: true })
		const ddata = await driverfiles_temp.findOneAndUpdate({ driverId: driver._id }, { driverId: data._id }, { new: true })
		await pendingLeaders.findOneAndUpdate({ _id: ddata.driverId }, { profileImage: ddata.driver_photo }, { new: true })
		await payoutMethod.findOneAndUpdate({ driverId: driver._id }, { driverId: data._id }, { new: true });
		return res.json({ code: '200', response: { driverId: ddata.driverId } });

	} catch (err) { return res.json({ code: "400", response: { msg: `${err.name} In pendingSignUp API => ${err.message}` } }) }
}

const driverDetails = async (req, res, next) => {
	try {
		let duration = 0;
		var h, m, s;
		let driverId = req.body.driverId;
		let data = await driverServices.getTodayDriverAnalytics(req.body.driverId);
		let loggedInLogs = await statusLogs.findOne({ driverId, createdAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } })
		if (loggedInLogs) {
			for (let i = 0; i < loggedInLogs.logs.length; i++) {
				if (loggedInLogs.logs[i].offTime == null) {
					let dur = moment.duration(moment(new Date()).diff(loggedInLogs.logs[i].onTime));
					duration = duration + dur.asMilliseconds();
				}
				else {
					duration = duration + loggedInLogs.logs[i].timeDifference;
				}
			}
		}
		let Details = await Driversignup.findById({ _id: driverId })
		let driverStatus = await DriverLocation.findOne({ driverId }, { status: 1 }).lean(true);
		if (!Details) { return res.json({ code: '400', response: { msg: 'Details not found' } }) }
		let VehDetails = await vehicleCards.findOne({ driverId: Details._id })
		if (!VehDetails) { return res.json({ code: '400', response: { msg: 'Veh Details not found' } }) }
		const { first_name, last_name, profileImage, rating, gender, city, email, licenseNumber } = Details;
		if (Details) {
			h = Math.floor(duration / 1000 / 60 / 60);
			m = Math.floor((duration / 1000 / 60 / 60 - h) * 60);
			s = Math.floor(((duration / 1000 / 60 / 60 - h) * 60 - m) * 60);
			s < 10 ? s = `0${s}` : s = `${s}`
			m < 10 ? m = `0${m}` : m = `${m}`
			h < 10 ? h = `0${h}` : h = `${h}`
			return res.json({
				code: '200', response: {
					first_name: first_name, last_name: last_name,
					profileImage: profileImage, gender: gender, email: email, licenseNumber: licenseNumber,
					city: city, rating: rating,
					todayDistance: data.data.todayDistance ? data.data.todayDistance : 0,
					todayEarned: data.data.todayEarning,
					todayRides: data.data.todayRides,
					onlineTime: `${h}:${m}:${s}`,
					vehicleName: `${VehDetails.categoryName}: ${VehDetails.registrationNo}`,
					status: driverStatus.status
				}
			})
		}
	} catch (err) { return res.json({ code: "400", response: { msg: `${err.name} In DriverDetails API => ${err.message}` } }) }
}
//////****** UpdateDriverInfo API ******///////
const updateDriverInfo = async (req, res) => {
	function decodeBase64Image(dataString) {
		let response = {};
		const base64Data = new Buffer.from(dataString.replace(/^data:image\/\w+;base64,/, ""), 'base64');
		const type = dataString.split(';')[0].split('/')[1];
		response.type = type;
		response.data = base64Data;
		return response;
	}
	try {
		const imgUpdateId = uniqueString();
		let Img = decodeBase64Image(req.body.driver_photo);
		const driverId = req.body.driverId;
		let fileRename = imgUpdateId + `_drv_Photo.${Img.type}`;
		const params = { Key: ('profileImg/' + fileRename), Body: Img.data, ContentEncoding: 'base64', ContentType: Img.type }
		const updateImg = await cloudBase.upload(params).promise();
		const imageUpdate = await driverCards.findOneAndUpdate({ driverId: driverId }, { driver_photo: updateImg.Location }, { new: true })
		if (!imageUpdate) { return res.json({ code: '400', response: { msg: 'Driver Picture is not updated' } }) }
		const imageId = imageUpdate.driverId;
		const { first_name, last_name, email, city, gender } = req.body;
		const driverUpdation = await Driversignup.findOneAndUpdate({ _id: imageId }, { first_name: first_name, last_name: last_name, email: email, city: city, gender: gender, profileImage: imageUpdate.driver_photo }, {
			new: true,
			fields: { 'first_name': 1, 'last_name': 1, 'email': 1, 'city': 1, 'gender': 1, 'profileImage': 1 }
		})
		if (!driverUpdation) { return res.json({ code: '400', response: { msg: 'Driver Picture is not updated' } }); }
		return res.json({ code: '200', response: driverUpdation })
	} catch (err) { return res.json({ code: "400", response: { msg: `${err.name} In updateDriverInfo API ${err.message}` } }) }
}
////// ****** VerifyPinCode Login API ****** //////
const verifyPinCode = async (req, res, next) => {
	try {
		const date = new Date(req.body.date);
		const verifyCode = await Driversignup.findOneAndUpdate({
			fullPhoneNumber: req.body.fullPhoneNumber,
			pinCode: req.body.pinCode
		}, {
			loggedInTime: new Date(),
			loggedIn: true
		}, { new: true });
		if (!verifyCode) {
			return res.json({
				code: '400',
				response: {
					msg: "PinCode or Number does not match"
				}
			});
		} else {
			verifyCode.userExists = 'true';
			await driverServices.statusOn(verifyCode._id, 'on')
			if (verifyCode.blockedState == false) {
				let VehDetails = await vehicleCards.findOne({
					driverId: verifyCode._id
				});
				var weekRide = await rideOrder.find({ driverId: verifyCode._id, rideStatus: 'rideCompleted', "orderCompletionTime": { "$gte": moment(new Date()).startOf('D').toISOString(), "$lt": moment(new Date()).endOf('D').toISOString() } },
					{ driverId: 1, rideStats: 1, orderCompletionTime: 1, rideStatus: 1 })
				let todayRides = 0
				let todayDistance = 0;
				let todayEarned = 0;
				if (weekRide) {
					weekRide.forEach(item => {

						todayDistance = todayDistance + item.rideStats.distanceInKm;
						todayRides++;
					});
					todayDistance = (Math.floor(todayDistance * 100)) / 100
				}
				else {
					return res.json({
						code: '400',
						response: {
							msg: 'error in getting orders'
						}
					});

				}
				let count = await Driversignup.find({ loggedIn: true, loggedInTime: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } }).countDocuments();
				console.log(count);
				let checkLog = await statusLogs.findOne({ driverId: verifyCode._id, createdAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } });
				if (!checkLog) {
					let saveLog = new statusLogs({
						driverId: verifyCode._id,
						logs: {
							onTime: new Date()
						}
					});
					await saveLog.save();
				}
				else {
					if (checkLog.logs[checkLog.logs.length - 1].offTime == null) {

					}
					else {
						let checkLog = await statusLogs.updateOne({ driverId: verifyCode._id, createdAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } }, { $push: { logs: { onTime: new Date() } } });

					}
				}
				request.post('http://13.251.166.77:2000/api/v1/master/addLeaderLogs', {
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						time: new Date().toISOString(),
						name: `${verifyCode.first_name} ${verifyCode.last_name}`,
						type: 'Leader',
						description: "Logged In",
						deviceName: req.body.deviceName,
						deviceIP: req.ip,
						deviceOS: req.body.deviceOS,
						countryId: verifyCode.countryId,
						cityId: verifyCode.cityId

					})
				}, (error, response) => {
					if (error) next(error)
				});
				const emailMsg = {
					to: `${verifyCode.email}`,
					from: `wowetsinc@gmail.com`,
					subject: `EMAIL LOG-IN INTIMATION`,
					html: `<strong><h4>Dear ${verifyCode.first_name} ${verifyCode.last_name},</h4></strong>
				    
				    <h4>You are successfully logged in to WOW at ${moment(req.body.date).format('hh:mm A')} on ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}. Enjoy the best electronic transport experience.
				    For any query call our helpline: <b>+971 45 217500</b> or email us at <span style="color:'#1036fc'"><u>info@wowets.com</u></span></h4>
				   <br> </br><br></br><br></br>
				    <p>
				    Disclaimer:<br></br>
					This email contains privileged and/or confidential information intended only for the use of the addressee. If you are not the intended recipient of this message, you are strictly prohibited to read, copy, distribute, discuss, disseminate its contents or take any action relying on the same and please notify the sender immediately. The recipient acknowledges that the sender is unable to exercise control or ensure or guarantee the integrity of/over the contents and/or information contained in this message, therefore, the WOWETS will not be liable for any errors or omissions which arise as a result of email transmission, any damages or errors whatever their nature, viruses, external influence, delays and the like.
				    </p>`
				}
				// sgMail.send(emailMsg).then(message => {
				// 	let say = 'hi'
				// }).catch(error => {
				// 	next(error);
				// });
				return res.json({
					code: '200',
					response: {
						_id: verifyCode._id,
						profileImage: verifyCode.profileImage,
						first_name: verifyCode.first_name,
						last_name: verifyCode.last_name,
						email: verifyCode.email,
						fullPhoneNumber: verifyCode.fullPhoneNumber,
						city: verifyCode.city,
						rating: (Math.floor(verifyCode.rating * 100)) / 100,
						todayEarned: `${todayEarned}`,
						todayDistance: `${todayDistance} Km`,
						todayRides: `${todayRides}`,
						nationality: verifyCode.nationality,
						vehicleName: `${VehDetails.categoryName}: ${VehDetails.registrationNo}`,
						licenseNumber: verifyCode.licenseNumber

					}
				})
			} else {
				return res.json({
					code: '400',
					response: {
						msg: 'Your account is blocked! Contact to admin.'
					}
				});
			}

		}
	} catch (err) {
		return res.json({
			code: "400",
			response: {
				msg: `${err.name} In verifyPinCode ${err.message}`
			}
		});
	}

}
////// ****** TouchIdLogin API ****** //////
const touchIdLogin = async (req, res) => {
	try {
		let driverId = req.body.driverId;
		let online = await driverServices.getOnlineTime(driverId);
		var weekRide = await rideOrder.find({ driverId: driverId, rideStatus: 'rideCompleted', "orderCompletionTime": { "$gte": moment(new Date()).startOf('D').toISOString(), "$lt": moment(new Date()).endOf('D').toISOString() } },
			{ driverId: 1, rideStats: 1, orderCompletionTime: 1, rideStatus: 1 })
		let todayRides = 0
		let todayDistance = 0;
		let todayEarned = 0;
		if (weekRide) {
			weekRide.forEach(item => {

				todayDistance = todayDistance + item.rideStats.distanceInKm;
				todayRides++;
			});
			todayDistance = (Math.floor(todayDistance * 100)) / 100
		}
		const reqId = await Driversignup.findOneAndUpdate({
			_id: driverId
		}, {
			loggedInTime: new Date(),
			loggedIn: true
		})
		if (!reqId) {
			return res.json({
				code: '400',
				response: {
					msg: 'DriverId does not exist'
				}
			});
		} else {
			req.body.userExists = 'true';
			if (reqId.blockedState == false) {
				let save = await driverServices.statusOn(driverId, 'on');
				console.log(save)
				let date = new Date();

				let VehDetails = await vehicleCards.findOne({
					driverId: reqId._id
				});
				let checkLog = await statusLogs.findOne({ driverId: reqId._id, createdAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } });
				if (!checkLog) {
					let saveLog = new statusLogs({
						driverId: reqId._id,
						logs: {
							onTime: new Date()
						}
					});
					await saveLog.save();
				}
				else {
					console.log('here')
					let checkLog = await statusLogs.updateOne({ driverId: reqId._id, createdAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } }, { $push: { logs: { onTime: new Date() } } });
				}
				request.post('http://13.251.166.77:2000/api/v1/master/addLeaderLogs', {
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						time: new Date().toISOString(),
						name: `${reqId.first_name} ${reqId.last_name}`,
						type: 'Leader',
						description: "Logged In",
						deviceName: req.body.deviceName,
						deviceIP: req.ip,
						deviceOS: req.body.deviceOS,
						countryId: reqId.countryId,
						cityId: reqId.cityId

					})
				}, (error, response) => {
					if (error) next(error)
				});
				const emailMsg = {
					to: `${reqId.email}`,
					from: `wowetsinc@gmail.com`,
					subject: `EMAIL LOG-IN INTIMATION`,
					html: `<strong><h4>Dear ${reqId.first_name} ${reqId.last_name},</h4></strong>
				    <h4>You are successfully logged in to WOW at ${moment(req.body.date).format('hh:mm A')} on ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}. Enjoy the best electronic transport experience.
				    For any query call our helpline: <b>+971 45 217500</b> or email us at <span style="color:'#1036fc'"><u>info@wowets.com</u></span></h4>
				   <br> </br><br></br><br></br>
				    <p>
				    Disclaimer:<br></br>
					This email contains privileged and/or confidential information intended only for the use of the addressee. If you are not the intended recipient of this message, you are strictly prohibited to read, copy, distribute, discuss, disseminate its contents or take any action relying on the same and please notify the sender immediately. The recipient acknowledges that the sender is unable to exercise control or ensure or guarantee the integrity of/over the contents and/or information contained in this message, therefore, the WOWETS will not be liable for any errors or omissions which arise as a result of email transmission, any damages or errors whatever their nature, viruses, external influence, delays and the like.
				    </p>`
				}
				// sgMail.send(emailMsg).then(message => {
				// 	let say = 'hi'
				// }).catch(error => {
				// 	next(error);
				// });
				return res.json({

					code: '200',
					response: {
						_id: reqId._id,
						profileImage: reqId.profileImage,
						first_name: reqId.first_name,
						last_name: reqId.last_name,
						email: reqId.email,
						fullPhoneNumber: reqId.fullPhoneNumber,
						city: reqId.city,
						rating: (Math.floor(reqId.rating * 100)) / 100,
						todayEarned: `${todayEarned}`,
						todayDistance: `${todayDistance} Km`,
						todayRides: `${todayRides}`,
						vehicleName: `${VehDetails.categoryName}: ${VehDetails.registrationNo}`,
						nationality: reqId.nationality,
						onlineTime: online.time,
						licenseNumber: reqId.licenseNumber
					}
				})
			} else {
				return res.json({
					code: '400',
					response: {
						msg: 'Your account is blocked! Contact to admin.'
					}
				});
			}
		}
	} catch (err) {
		return res.json({
			code: "400",
			response: {
				msg: `${err.name} In touchIdLogin API ${err.message}`
			}
		});
	}

}
////// ****** Logout API ****** //////
const logout = async (req, res, next) => {
	let driverId = req.body.driverId;
	try {
		await request.post('http://18.138.89.166:9003/api/v1/rider/rideorder/isDriverOrderPending', {
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ driverId })
		}, async (error, response, body) => {
			if (error) {
				res.statusCode = 400
				res.json({ code: '400', response: { message: "an error occured in customer api" } })
			}
			else if (response.statusCode == 200) {
				let checkOrder = JSON.parse(body);
				if (checkOrder.response == true) {
					res.statusCode = 200
					res.json({ code: '400', response: { message: "You have pending order" } })
				}
				if (checkOrder.response == false) {
					let logout = await Driversignup.findByIdAndUpdate({ _id: driverId }, { loggedIn: false, loggedOutTime: new Date() }, { new: true });
					if (logout) {
						let offDriverLocation = await DriverLocation.findOneAndUpdate({ driverId: driverId }, { status: 'off' }, { new: true });
						if (offDriverLocation) {
							let findLogs = await statusLogs.findOne({ driverId, createdAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } });
							if (!findLogs) {
								let nothing = null
							}
							else {
								findLogs.logs[findLogs.logs.length - 1].offTime = new Date();
								let off = moment.duration(moment(findLogs.logs[findLogs.logs.length - 1].offTime).diff(moment(findLogs.logs[findLogs.logs.length - 1].onTime))).asMilliseconds();
								findLogs.logs[findLogs.logs.length - 1].timeDifference = off;
								findLogs.save();
							}
							request.post('http://13.251.166.77:2000/api/v1/master/addLeaderLogs', {
								headers: {
									'Content-Type': 'application/json'
								},
								body: JSON.stringify({
									time: new Date().toISOString(),
									name: `${logout.first_name} ${logout.last_name}`,
									type: 'Leader',
									description: "Logged Out",
									deviceName: req.body.deviceName,
									deviceIP: req.ip,
									deviceOS: req.body.deviceOS,
									countryId: logout.countryId,
									cityId: logout.cityId

								})
							}, (error, response) => {
								if (error) next(error)
							});
							res.statusCode = 200
							res.json({ code: '200', response: { message: "Successfully Logout" } })
						}
						else if (!offDriverLocation) {
							res.statusCode = 400
							res.json({ code: '400', response: { message: "no driver found" } })
						}
					}
					else if (!logout) {
						res.statusCode = 400
						res.json({ code: '400', response: { message: "no driver found" } })
					}

				}
			}
		})
	}
	catch (error) {
		next(error)
	}

}
////// ****** RefreshDeviceToken API ****** //////
const refreshDeviceToken = async (req, res) => {
	try {
		let driverId = req.body.driverId; let deviceToken = req.body.deviceToken;
		const driver = await Driversignup.findOneAndUpdate({ _id: driverId }, { deviceToken: deviceToken }, { new: true })
		if (!driver) { return res.json({ code: '400', response: { msg: 'Refresh token is failed' } }) }
		return res.json({ code: '200', response: { msg: 'Device token is sucessfully refreshed' } })
	} catch (err) { return res.json({ code: "400", response: { msg: `${err.name} In refreshDeviceToken API ${err.message}` } }) }
};
module.exports = { temporarySignUp, refreshDeviceToken, pendingSignUp, logout, verifyPinCode, touchIdLogin, driverDetails, updateDriverInfo };