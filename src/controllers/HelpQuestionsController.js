const dialogflow = require('dialogflow');
const uuid = require('uuid');
const pusher = require('../util/pusher');
const creds = require('../config/leaderQuestionsAndAnswers-7ca015d6df26.json');
const questionClass = require('../Services/helpQuestions');
const questions = new questionClass();
const sessionId = uuid.v4();
export const HelpQuestionsAndAnswer = async (req, res, next) => {
  try {
    let driverId = req.body.driverId;
    let question = req.body.question;
    await questions.saveChatbotHistory(driverId,driverId, 'wow', question);
    const sessionClient = new dialogflow.SessionsClient({ credentials: creds });
    const sessionPath = sessionClient.sessionPath('leaderquestionsandanswers-mfef', sessionId);
    const request = {
      session: sessionPath,
      queryInput: {
        text: {
          text: req.body.question,
          languageCode: 'en-US',
        }
      },
    };

    const responses = await sessionClient.detectIntent(request);
    if(responses){
      const result = responses[0].queryResult;
      pusher.trigger(driverId, 'helpQuestionResponse', {
        answer: result.fulfillmentText
      });
      await questions.saveChatbotHistory(driverId,'wow',driverId,  result.fulfillmentText);
      return res.json({
        code: '200', response: {
          driverId: driverId,
          question: req.body.question
        }
      });
    }
    

  } catch (error) {
    next(error);
  }
}
export const addNewQuestion = async (req, res, next) => {
  try {
    const {
      question,
      link
    } = req.body;
    let saveNewQuestion = await questions.addQuestionsForHelp(question, link);
    if (saveNewQuestion) return res.status(200).json({ code: '200', response: { msg: 'new question added successfully' } });
  } catch (error) {
    next(error)
  }
}
export const showAllHelpQuestions = async (req, res, next) => {
  try {
    let question = await questions.getAllQuestions();
    if (question) return res.status(200).json({ code: '200', response:question });
    
  } catch (error) {
    next(error);
  }
}
export const showChatbotHistory = async (req,res,next)=>{
  try {
    let getHistory = await questions.getChatbotHistory(req.body.driverId);
    return res.status(200).json({code:'200',response:getHistory});
  } catch (error) {
    next(error);
  }
}


