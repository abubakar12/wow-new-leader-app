import { get } from 'mongoose';

const axios = require('axios');
const ratings = require('../Services/ratings');
const leaderRating = new ratings();

export const getDriverRating = async (req, res, next) => {
         try {
                  let driverId = req.params.driverId;
                  let page = req.params.page;
                  let limit = req.params.limit;
                  let getRating = await leaderRating.getRating(driverId, page, limit);
                  for (let i = 0; i < getRating.docs.length; i++) {
                           let customer = await leaderRating.getCustomerProfile(getRating.docs[i].customer.customerId);
                           getRating.docs[i].driver.fullName = customer.fullName;
                           getRating.docs[i].driver.profileImage = customer.profileImage;
                  }
                  res.status(200).json({ code: '200', response: getRating });
         } catch (error) {
                  next(error);
         }
}