const notificationClass = require('../Services/notifications');
const notifications = new notificationClass();

export const saveNewNotification = async (req, res, next) => {
         try {
                  const { driverId, title, body, type } = req.body;
                  let saveNotification = await notifications.saveNewNotification(driverId, title, body, type);
                  return res.status(200).json({ code: '200', response: { msg: 'notification saved' } });
         } catch (error) {
                  next(error);
         }
}

export const getLeaderNotification = async (req, res, next) => {
         try {
                  let retrieve = await notifications.getLeaderNotifications(req.body.driverId);
                  if (!retrieve) { return res.status(200).json({ code: '200', response: [] }) }
                  else{return res.status(200).json({ code: '200', response: retrieve })}


         } catch (error) {
                  next(error)
         }
}