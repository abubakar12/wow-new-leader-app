const adminService = require('../Services/AdminAPIs');
const MasterAdmin = new adminService();

const getAllApprovedLeaders = async (req, res, next) => {
	try {
		let { countryId, cityId, startDate, endDate } = req.body;
		const leaders = await MasterAdmin.getApprovedLeadersByAdmin(countryId, cityId, startDate, endDate, req.params.page, req.params.limit);
		return res.status(200).json({ code: "200", response: leaders });
	} catch (err) { next(err) }
};
const getAllBlockedLeaders = async (req, res, next) => {
	try {
		let { countryId, cityId, startDate, endDate } = req.body;
		const leaders = await MasterAdmin.getBlockedLeadersByAdmin(countryId, cityId, startDate, endDate, req.params.page, req.params.limit);
		return res.status(200).json({ code: "200", response: leaders });
	} catch (err) { next(err) }
};
const getAllPendingLeaders = async (req, res, next) => {
	try {
		const { countryId, cityId, startDate, endDate } = req.body;
		let page = req.params.page; let limit = req.params.limit;
		let leaders = await MasterAdmin.getPendingLeadersByAdmin(countryId, cityId, startDate, endDate, page, limit);
		return res.status(200).json({ code: '200', response: leaders });
	} catch (error) { next(error) }
};
const getAllDisApprovalLeaders = async (req, res, next) => {
	try {
		let { countryId, cityId, startDate, endDate } = req.body;
		const leaders = await MasterAdmin.getDisApprovedLeadersByAdmin(countryId, cityId, startDate, endDate, req.params.page, req.params.limit);
		console.log(leaders);
		return res.status(200).json({ code: "200", response: leaders });
	} catch (err) { next(err) }
};
const getActiveLeaders = async (req, res, next) => {
	try {
		let { countryId, cityId, startDate, endDate } = req.body;
		let page = req.params.page; let limit = req.params.limit;
		let leaders = await MasterAdmin.getActiveLeadersByAdmin(countryId, cityId, startDate, endDate, page, limit);
		return res.status(200).json({ code: '200', response: leaders });
	} catch (err) { next(err) }
};
const getInActiveLeaders = async (req, res, next) => {
	try {
		let { countryId, cityId, startDate, endDate } = req.body;
		let page = req.params.page; let limit = req.params.limit;
		let leaders = await MasterAdmin.getInActiveLeadersByAdmin(countryId, cityId, startDate, endDate, page, limit);
		return res.status(200).json({ code: '200', response: leaders });
	} catch (err) { next(err) }
};
const getAllDetails = async (req, res, next) => {
	try {
		let leaders = await MasterAdmin.getAllLeaderDetailsByAdmin(req.body.driverId);
		return res.status(200).json({ code: "200", response: leaders });
	} catch (err) { next(err) }
};
const approvedToBlocked = async (req, res, next) => {
	try {
		let leaders = await MasterAdmin.approveToBlockByAdmin(req.body.driverId, req.body.blockedReason);
		return res.status(200).json({ code: "200", response: leaders });
	} catch (err) { next(err) }
};
const blockedToApproved = async (req, res, next) => {
	try {
		let leaders = await MasterAdmin.blockToApproveByAdmin(req.body.driverId);
		return res.status(200).json({ code: "200", response: leaders });
	} catch (err) { next(err) }
}
const pendingToApproved = async (req, res, next) => {
	try {
		let leaders = await MasterAdmin.leaderApproveByAdmin(req.body.driverId);
		return res.status(200).json({ code: "200", response: leaders });
	} catch (err) { next(err) }
};
const pendingToDisApproval = async (req, res, next) => {
	try {
		let leaders = await MasterAdmin.leaderDisApproveByAdmin(req.body.driverId, req.body.disApprovalReason);
		return res.status(200).json({ code: "200", response: leaders });
	} catch (err) { next(err) }
};
const disApprovedToPending = async (req, res, next) => {
	try {
		let leaders = await MasterAdmin.disApproveToPendingByAdmin(req.body.driverId);
		return res.status(200).json({ code: "200", response: leaders });
	} catch (err) { next(err) }
}
const getAllOnlineLeaders = async (req, res, next) => {
	try {
		let { countryId, cityId, startDate, endDate } = req.body;
		let page = req.params.page; let limit = req.params.limit;
		let leaders = await MasterAdmin.getOnlineLeaderByAdmin(countryId, cityId, startDate, endDate, page, limit);
		return res.status(200).json({ code: '200', response: leaders });
	} catch (err) { next(err) }
}
const getCarsCount = async (req, res, next) => {
	try {
		let response = await MasterAdmin.getCarsCounts();
		res.statusCode = 200
		res.json({ code: '200', response });
	} catch (error) { next(error) }
}

const getDriverSearchingCars = async (req,res,next)=>{
try {
	let cars = await MasterAdmin.getSearchingCarsByAdmin(req.body.latitude,req.body.longitude);
	res.status(200).json({code:'200',searchingCars:cars});
} catch (error) {
	next(error);
}	

}
module.exports = { getCarsCount, disApprovedToPending, blockedToApproved, getAllApprovedLeaders, getAllBlockedLeaders, getAllPendingLeaders, getActiveLeaders, getInActiveLeaders, getAllDisApprovalLeaders, getAllDetails, pendingToApproved, approvedToBlocked, pendingToDisApproval, getAllOnlineLeaders,getDriverSearchingCars };