const vehicleCategory = require('../models/vehicleCategory');
const vehicleCards = require('../models/AuthNVehicle');

const getAllCategoryNames = async (req, res) => {
	try {
		const categoriesNames = await vehicleCategory.find({}, 'categoryName')
		if (!categoriesNames) { return res.json({ code: '400', response: { msg: 'An Error Occured' } }) }
		return res.json({ code: '200', response: categoriesNames });
	} catch (err) { return res.json({ code: "400", response: { msg: `${err.name} In getAllCategoryNames API ${err.message}` } }) }
}

const getVehiclesByCategory = async (req, res) => {
	try {
		const vehicleNames = await vehicleCategory.findOne({ categoryName: req.body.categoryName }, 'vehicles')
		if (!vehicleNames) { return res.json({ code: '400', response: { msg: 'An Error Occured' } }) }
		return res.json({ code: '200', response: vehicleNames });
	} catch (err) { return res.json({ code: "400", response: { msg: `${err.name} In getVehiclesByCategory API ${err.message}` } }) }
}

const getVehicleByDriverId = async (req, res) => {
	try {
		const driverId = req.body.driverId
		const details = await vehicleCards.findOne({ driverId: driverId }).select({ _id: 0, vehicleId: 1, categoryId: 1 })
		if (!details) { return res.json({ code: '400', response: { msg: 'An error occured' } }) }
		return res.json({ code: '200', response: details })
	} catch (err) { return res.json({ code: "400", response: { msg: `${err.name} In getVehicleByDriverId API ${err.message}` } }) }
}

const getListOfDriverVehicles = async (req, res) => {
	try {
		const driverId = req.body.drivers
		const allDetails = await vehicleCards.find({ driverId: driverId }, 'vehicleId categoryId -_id')
		if (!allDetails) { return res.json({ code: '400', response: { msg: 'An error occured' } }) }
		return res.json({ code: '200', response: allDetails })
	} catch (err) { return res.json({ code: "400", response: { msg: `${err.name} In getListOfDriverId API ${err.message}` } }) }
};
module.exports = { getAllCategoryNames, getVehiclesByCategory, getVehicleByDriverId, getListOfDriverVehicles };