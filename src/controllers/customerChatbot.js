const dialogflow = require('dialogflow');
const uuid = require('uuid');
const pusher = require('../util/pusher');
const creds = require('../config/customerQuestionsAndAnswer-2ecec9ad43cf.json');
const sessionId = uuid.v4();
export const chatBotForCustomer = async (req, res, next) => {
         try {
                  let customerId = req.body.customerId;
                  let question = req.body.question;
                  const sessionClient = new dialogflow.SessionsClient({ credentials: creds });
                  const sessionPath = sessionClient.sessionPath('customerquestionsandanswer-tkg', sessionId);
                  pusher.trigger(customerId, 'chatBot', {
                           senderId: customerId,
                           body: question
                  });
                  const request = {
                           session: sessionPath,
                           queryInput: {
                                    text: {
                                             text: question,
                                             languageCode: 'en-US',
                                    }
                           },
                  };

                  const responses = await sessionClient.detectIntent(request);
                  if (responses) {
                           const result = responses[0].queryResult;
                           pusher.trigger(customerId, 'chatBot', {
                                    senderId: sessionId,
                                    body: result.fulfillmentText
                           });
                           return res.json({
                                    code: '200', response: {
                                             customerId: customerId,
                                             question: question
                                    }
                           });
                  }
         } catch (error) {
                  next(error);
         }
}