const mongoose = require('mongoose');

const driverfiles_schema = mongoose.Schema({
	driverId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'signUpTemp'
	},
	driver_photo: {
		type: String,
		required: true
	},
	emirates_card_f: {
		type: String,
		required: true
	},
	expiry_date_emirates: {
		type: String
	},
	emirates_card_b: {
		type: String,
		required: true
	},
	rta_card_f: {
		type: String,
		required: true
	},
	expiry_date_rta: {
		type: String
	},
	rta_card_b: {
		type: String,
		required: true
	},
	rta_number:{
		type:String,
		default:''
	}
}, {
	timestamps: true,
	collection: 'DriverDocuments'
});

module.exports = mongoose.model('driverfiles', driverfiles_schema);