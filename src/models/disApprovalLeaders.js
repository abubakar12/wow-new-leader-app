const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');
const disApprovalLeadersSchema = new Schema({
    driverId: { type: mongoose.Schema.Types.ObjectId, ref: 'Driversignup' },
    disApprovalReason: { type: String, required: true },
    disapprovedState: { type: Boolean },
}, { timestamps: true, collection: 'DisApproveDrivers' });
disApprovalLeadersSchema.plugin(mongoosePaginate);
const disApprovalLeaders = mongoose.model('disApprovalLeaders', disApprovalLeadersSchema);
module.exports = disApprovalLeaders;