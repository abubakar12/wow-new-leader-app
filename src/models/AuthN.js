const mongoose = require('mongoose');
// Mongoose-Paginate
const mongoosePaginate = require('mongoose-paginate-v2');
const ridePreferenceSchema = mongoose.Schema({
	baggage: { type: Number, required: [true, 'Enter baggage detail'] },
	passenger: { type: Number, required: [true, 'Enter passenger detail'] },
	childSeat: { type: Number, required: [true, 'Enter child seats'] },
	rideAssistance: { type: Boolean, required: [true, 'Enter ride assistance'] },
	drinkingWater: { type: Boolean, required: [true, 'Enter drinking water'] },
	temperature: { type: String, required: [true, 'Enter your temperature'] }
});
const driversignup_schema = mongoose.Schema({
	first_name: { type: String, required: true },
	last_name: { type: String, required: true },
	email: { type: String, lowercase: true, required: [true, 'Email is required'] },
	city: { type: String},
	countryId: { type: String },
	cityId: { type: String },
	countryCode: { type: String, required: true },
	phoneNumber: { type: String, index: true, required: true },
	fullPhoneNumber: { type: String, index: true, unique: true, required: true },
	pinCode: { type: String },
	gender: { type: String },
	invite_code: { type: String },
	emiratesID: { type: String, },
	licenseNumber: { type: String, required: [true, 'Please enter the license number'] },
	rating: { type: Number, required: [true, 'Must have a rating'], default: '0.0' },
	deviceToken: { type: String, default: true },
	loggedIn: { type: Boolean },
	loggedInTime: { type: Date },
	loggedOutTime: { type: Date },
	blockedTime: { type: Date },
	approveTime: { type: Date },
	blockedState: { type: Boolean, default: false },
	profileImage: { type: String, default: true },
	nationality: String,
	preferences: { type: ridePreferenceSchema }
},
	{
		timestamps: true,
		collection: 'Drivers'
	});
driversignup_schema.plugin(mongoosePaginate);
module.exports = mongoose.model('Driversignup', driversignup_schema);