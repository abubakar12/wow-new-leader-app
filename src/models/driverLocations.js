const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DriversStatusSchema = new Schema({
	driverId: {
		type: String,
		unique: true,
		required: [true, 'Driver ID is required']
	},
	batteryStatus: {
		type: String,
		required: [true, 'Battery Status Is Required Include That']
	},
	// prevLocation: {
	// 	type: {
	// 		type: String,
	// 		enum: ['Point'],
	// 		default: 'Point'
	// 	},
	// 	coordinates: {
	// 		type: [Number],
	// 		required: true
	// 	}
	// },
	location: {
		type: {
			type: String,
			enum: ['Point'],
			default: 'Point'
		},
		coordinates: {
			type: [Number],
			required: true
		}
	},
	status: {
		type: String,
		enum: ['off', 'on', 'engaged','busy'],
		default: 'on'
	}
}, {
	timestamps: true,
	collection: 'DriverLocations'
});

DriversStatusSchema.index({
	location: '2dsphere'
});
DriversStatusSchema.methods.findNearByDriversByStatus = async function (status) {
	return await this.model('DriverLocation').find({
			status: status,
			location: {
				$nearSphere: {
					$geometry: {
						type: 'Point',
						coordinates: [this.location.coordinates[0], this.location.coordinates[1]]
					},
					$maxDistance: 5000
				}
			}
		},{driverId:1,batteryStatus:1,location:1,status:1}
	);
};

DriversStatusSchema.methods.findNearByDriversByRadius = async function (status, radius) {
	let max_distance = radius * 1000;
	return await this.model('DriverLocation').find({
			status: status,
			location: {
				$nearSphere: {
					$geometry: {
						type: 'Point',
						coordinates: [this.location.coordinates[0], this.location.coordinates[1]]
					},
					$maxDistance: max_distance
				}
			}
		},{driverId:1,batteryStatus:1,location:1,status:1}
	).lean();
};
DriversStatusSchema.methods.countNearByDriversByRadius = async function (status, radius) {
	let max_distance = radius * 1000;
	return await this.model('DriverLocation').find({
			status: status,
			location: {
				$nearSphere: {
					$geometry: {
						type: 'Point',
						coordinates: [this.location.coordinates[0], this.location.coordinates[1]]
					},
					$maxDistance: max_distance
				}
			}
		}
	).count();
};

DriversStatusSchema.methods.findNearByDrivers = function (cb) {
	return this.model('DriverLocation').find({
			location: {
				$nearSphere: {
					$geometry: {
						type: 'Point',
						coordinates: [this.location.coordinates[0], this.location.coordinates[1]]
					},
					$maxDistance: 5000
				}
			}
		},{driverId:1,batteryStatus:1,location:1,status:1},
		cb
	);
};

module.exports = mongoose.model('DriverLocation', DriversStatusSchema);