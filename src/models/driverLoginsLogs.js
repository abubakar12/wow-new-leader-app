const mongoose = require('mongoose');
const login = mongoose.Schema({
    onTime:{
        type:Date,
        default:null
    },
    offTime:{
        type:Date,
        default:null
    },
    timeDifference:{
        type:Number
    }
})
const loginLogs = mongoose.Schema({
    driverId:{
        type:String,
        required:true
    },
    logs:{
        type:[login]
    }

},
{timestamps: true,
    collection: 'DriverLoginLogs' });
module.exports = mongoose.model('loginLogs', loginLogs);