const mongoose = require('mongoose');

const vehiclefiles_schema = mongoose.Schema({
	driverId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'signUpTemp'
	},
	categoryId: {
		type: String,
		required: true,
		trim: true
	},
	vehicleId: {
		type: String,
		required: true,
		trim: true
	},
	registrationNo: {
		type: String,
		required: true
	},
	year: {
		type: String,
		required: true
	},
	color: {
		type: String,
		required: true
	},
	expiryDate: {
		type: String
	},
	vehicle_reg_f: {
		type: String,
		required: true
	},
	vehicle_reg_b: {
		type: String,
		required: true
	},
	categoryName: {
		type: String,
	},
	vehicleName: {
		type: String,
	}
}, {
	timestamps: true,
	collection: 'DriverVehicleDocuments'
});
module.exports = mongoose.model('vehiclefiles', vehiclefiles_schema);