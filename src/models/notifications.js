const mongoose = require('mongoose');

let notifications = [
    "Promo",
    "Updates",
    "Schedule Ride"
]
const schema = new mongoose.Schema({
    driverId: {
        type: String,
        required: true
    },
    notification: {
        title: { type: String },
        body: { type: String },
        notificationType: { type: String , enum:notifications}
    }
}, {
    timestamps: true,
    collection: 'DriverNotifications'
});
module.exports = mongoose.model('LeaderNotifications',schema,'LeaderNotifications');