const mongoose = require('mongoose');
const SignUpTempSchema = mongoose.Schema({
	first_name: {
		type: String,
		required: [true, 'First Name is required']
	},
	last_name: {
		type: String,
		required: [true, 'Last Name is required']
	},
	email: {
		type: String,
		lowercase: true,
		required: [true, 'Email is required']
	},
	countryCode: {
		type: String,
		trim: true
	},
	phoneNumber: {
		type: String,
		index: true,
		trim: true
	},
	fullPhoneNumber: {
		type: String,
		index: true,
		unique: true,
		trim: true
	},
	emiratesID: {
		type: String,
	},
	gender: {
		type: String,
		default: 'Male'
	},
	licenseNumber: {
		type: String,
		required: [true, 'Please enter the license number']
	},
	countryId: {
		type: String,
		trim: true
	},
	cityId: {
		type: String,
		trim: true
	},
	invite_code: {
		type: String,
	},
	nationality: {
		type: String,
		required: [true, 'Nationality required']
	}
}, {
	timestamps: true,
	collection: 'DriverTempSignUp'
});
const SignUpTemp = mongoose.model('signUpTemp', SignUpTempSchema);
module.exports = SignUpTemp;