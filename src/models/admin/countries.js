const {
    master_conn
} = require('../../config/DBConnections');
const mongoose = require('mongoose');
// const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;
const countriesSchema = new Schema({
    shortName: {
        type: String,
        unique: true
    },
    name: {
        type: String,
        unique: true
    },
    phone: {
        type: String
    },
    capital: {
        type: String
    },
    currency: {
        type: String
    },
    languages: {
        type: String
    },
    active: {
        type: Boolean,
        default: false
    }
});
// countriesSchema.plugin(mongoosePaginate);
const Country = master_conn.model('countries', countriesSchema);
module.exports = Country