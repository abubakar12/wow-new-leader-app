const mongoose = require('mongoose');

const Schema = mongoose.Schema;
// Aggregate-Paginate
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const DriverOrderResponseSchema = new Schema({
	orderId: {
		type: String,
	//	index: true,
		required: [true, 'Order ID is required']
	},
	userId: {
		type: String,
	//	index: true,
	//	required: [true, 'User ID is required']
	},
	driverId: {
		type: String,
		index: true,
		required: [true, 'Driver ID is required']
	},
	timeLabel: {
		type: String,
	//	required: [true, 'Time Label is required']
	},
	timeInSeconds: {
		type: Number,
	//	required: [true, 'Time in Seconds is required']
	},
	distanceInKmLabel: {
		type: String,
		//required: [true, 'Distance In Km Label is required']
	},
	distanceInMeters: {
		type: Number,
		//required: [true, 'Distance In Number Label is required']
	},
	driverBatteryStatus:{
		type: String,
		required: [true, 'Battery Status Is Required Include That']
	},
	driverLocation: {
		type: {
			type: String,
			enum: ['Point'],
		//	default: 'Point'
		},
		coordinates: {
			type: [Number],
		//	required: true
		}
	},
	pickupLocation: {
		type: {
			type: String,
			enum: ['Point'],
		//	default: 'Point'
		},
		coordinates: {
			type: [Number],
		//	required: true
		}
	},
	destinationLocation: {
		type: {
			type: String,
			enum: ['Point'],
			//default: 'Point'
		},
		coordinates: {
			type: [Number],
		//	required: true
		}
	},
	estimatedFareRange: {
		type: String,
		//required: [true, 'Estimated Fare Range is required']
	},
	currency: {
		type: String,
		enum: ['AED'],
		//default: 'AED',
		//required: [true, 'Currency is required']
	},
	sendTime: {
		type: Date,
		default: Date.now
	},
	acceptTime: {
		type: Date
	},
	rejectTime: {
		type: Date
	},
	orderSaveTime:{
		type:Date
	},
	status: {
		type: String,
		enum: ['pending','accepted','rejected','timePassed','cancel'],
		default: 'pending'
	},
	isCancel:{
		type:Boolean
	},
	cancelBy:{
		type:String
	}
}, {
	timestamps: true
});

DriverOrderResponseSchema.index({
	pickupLocation: '2dsphere',
	destinationLocation: '2dsphere',
	driverLocation:'2dsphere'
});

DriverOrderResponseSchema.plugin(mongooseAggregatePaginate);

module.exports = mongoose.model('DriverOrderResponse', DriverOrderResponseSchema);