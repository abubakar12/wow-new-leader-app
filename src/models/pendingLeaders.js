const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;
const PendingLeadersSchema = new Schema({
	first_name: {
		type: String,
		required: [true, 'First Name is required']
	},
	last_name: {
		type: String,
		required: [true, 'Last Name is required']
	},
	email: {
		type: String,
		lowercase: true,
		required: [true, 'Email is required']
	},
	emiratesID: {
		type: String,
	},
	licenseNumber: {
		type: String,
		required: [true, 'Please enter the license number']
	},
	countryId: {
		type: String
	},
	cityId: {
		type: String
	},
	countryCode: {
		type: String,
	},
	phoneNumber: {
		type: String,
		index: true,
		unique: true
	},
	fullPhoneNumber: {
		type: String,
		index: true,
		unique: true
	},
	gender: {
		type: String,
		default: 'Male'
	},
	invite_code: {
		type: String
	},
	pinCode: {
		type: String,
		default: '1234'
	},
	profileImage: {
		type: String,
		default: true
	},
	disapprovedState: {
		type: Boolean,
		default: false
	},
	nationality: {
		type: String
	}
}, { timestamps: true, collection: 'PendingDrivers' });
PendingLeadersSchema.plugin(mongoosePaginate);
const PendingLeaders = mongoose.model('pendingLeaders', PendingLeadersSchema);
module.exports = PendingLeaders;