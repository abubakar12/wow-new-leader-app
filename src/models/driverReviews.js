const mongoose = require('mongoose');
const orderSchema = mongoose.Schema({
    driverId: {
        type: String
    },
    customerProfile: {
        type: String
    },
    customerName: {
        type: String
    },
    customerRating: {
        type: Number
    },
    customerComment: {
        type: String
    }
},
    { timestamps: true });
module.exports = mongoose.model('driverReviews', orderSchema);