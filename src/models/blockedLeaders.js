// const {
// 	driver_conn
// } = require('../../config/DBConnections');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');
// const ObjectId = mongoose.Schema.Types.ObjectId;
const BlockedLeadersSchema = new Schema({
	driverId: { type: mongoose.Schema.Types.ObjectId, ref: 'Driversignup' },
	blockedReason: { type: String, required: true },
	blockedState: { type: Boolean },
}, { timestamps: true, collection: 'BlockDrivers' });
BlockedLeadersSchema.plugin(mongoosePaginate);
const BlockedLeaders = mongoose.model('blockedLeaders', BlockedLeadersSchema);
module.exports = BlockedLeaders;