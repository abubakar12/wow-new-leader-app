const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const orderSchema = mongoose.Schema({
    driverId: {
        type: String
    },
    customerId: {
        type: String
    },
    orderRating: {
        type: Number
    },
    amount: {
        type: Number
    },
    distance: {
        type: Number
    },
    pickLocation: {
        type: String
    },
    pickTime: {
        type: String
    },
    dropLocation: {
        type: String
    },
    dropTime: {
        type: String
    },
    date: {
        type: String
    },
    tip: {
        type: Number
    },
    tollGate: {
        type: Number
    },
    parking: {
        type: Number
    },
    cancelRide: {
        type: Number
    },
    bonus: {
        type: Number
    },
},
    { timestamps: true });
orderSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('orderHistory', orderSchema);