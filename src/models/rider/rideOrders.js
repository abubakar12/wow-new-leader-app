const mongoose = require("mongoose");
const { rider_conn } = require('../../config/DBConnections');
const Schema = mongoose.Schema;

const rideStatus = [
  "driverSearching",
  "driverAccepted",
  "driverArrived",
  "rideStarted",
  "rideCompleted"
];

export const RideOrdersSchema = new Schema({
  customerId: { type: String, required: true },
  pickupAddress: { type: String, required: true },
  pickupLocation: {
    type: { type: String, enum: ['Point'], default: 'Point' },
    coordinates: { type: [Number], required: true }
  },
  destinationAddress: { type: String },
  destinationLocation: {
    type: { type: String, enum: ['Point'] },
    coordinates: { type: [Number] }
  },
  estimatedStats: {
    destinationAddress: { type: String },
    destinationLocation: {
      type: { type: String, enum: ['Point'] },
      coordinates: { type: [Number] }
    },
    currency: { type: String },
    timeLabel: { type: String },
    timeInMinutes: { type: Number },
    distanceInKmLabel: { type: String },
    distanceInKm: { type: Number },
    estimatedFareStartRange: { type: String },
    estimatedFareEndRange: { type: String }
  },
  ridePreferences: {
    water: { type: Boolean },
    baggage: { type: Boolean },
    temperature: { type: Boolean },
    rideAssistant: { type: Boolean }
  },
  categoryId: { type: String, required: true },
  vehicleId: { type: String, required: true },
  driverId: { type: String },
  rideStatus: { type: String, enum: rideStatus, default: 'driverSearching' },
  orderStatus: { type: String, enum: ['pending','completed'] , default: 'pending' },
  isCancelled: { type: Boolean, default: false },
  cancelledBy: { type: String, enum: ['customer','driver','system'] },
  cancelReason: { type: String },
  cancelTime: { type: Date },
  acceptTime: { type: Date },
  arrivalTime: { type: Date },
  rideStartTime: { type: Date },
  rideCompletionTime: { type: Date },
  orderStartTime: { type: Date, default: Date.now },
  orderCompletionTime: { type: Date },
  paymentMethod: {
    isWallet: { type: Boolean, default: true },
    type: { type: String, enum: ['cash','card'] },
    methodId: { type: String }
  },
  rideStats: {
    timeLabel: { type: String },
    timeInSeconds: { type: Number },
    timeInMinutes: { type: Number },
    distanceLabel: { type: String },
    distanceInMeters: { type: Number },
    distanceInKm: { type: Number }
  },
});

RideOrdersSchema.set('toObject', { versionKey: false });

RideOrdersSchema.set('toJSON', { versionKey: false });

module.exports = rider_conn.model('RideOrders', RideOrdersSchema, 'RideOrders');
