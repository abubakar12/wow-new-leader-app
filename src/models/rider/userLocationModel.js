const { rider_conn } = require('../../config/DBConnections');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UsersLocationSchema = new Schema(
	{
		userId: {
			type: String,
			unique: true,
			required: [ true, 'User ID is required' ]
		},
		batteryStatus: {
			type: String,
			default: '50'
		},
		location: {
			type: {
				type: String,
				enum: [ 'Point' ],
				default: 'Point'
			},
			coordinates: {
				type: [ Number ],
				required: true
			}
		}
	},
	{ timestamps: true }
);

UsersLocationSchema.index({ location: '2dsphere' });

UsersLocationSchema.methods.findNearByUsers = function(cb) {
	return this.model('UserLocation').find(
		{
			location: {
				$geoWithin: {
					$centerSphere: [ this.location.coordinates, 5 / (1.609344 * 3963.2) ]
				}
			}
		},
		cb
	);
};

const UserLocation = rider_conn.model('UserLocation', UsersLocationSchema);
module.exports = UserLocation;
