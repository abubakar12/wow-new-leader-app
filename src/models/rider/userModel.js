const { rider_conn } = require('../../config/DBConnections');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
	userName: {
		type: String,
		lowercase: true,
		required: [ true, 'Username is required' ]
	},
	firstName: {
		type: String,
		required: [ true, 'First Name is required' ]
	},
	lastName: {
		type: String,
		required: [ true, 'Last Name is required' ]
	},
	email: {
		type: String,
		lowercase: true,
		required: [ true, 'Email is required' ]
	},
	countryCode: {
		type: String,
		required: [ true, 'Country Code is required' ]
	},
	phoneNumber: {
		type: String,
		unique: true,
		required: [ true, 'Phone Number is required' ]
	},
	fullPhoneNumber: {
		type: String,
		unique: true,
		required: [ true, 'Full Phone Number is required' ]
	},
	hashPassword: {
		type: String,
		required: true
	},
	rating: {
		type: Number,
		default: 3.0
	},
	profile_image: {
		type: String
	},
	loggedIn: {
		type: Boolean,
		default: true
	},
	createdDate: {
		type: Date,
		default: Date.now
	}
});

UserSchema.methods.setPassword = function(password) {
	const saltRounds = 10;
	this.hashPassword = bcrypt.hashSync(password, saltRounds);
};

UserSchema.methods.comparePassword = (password, hashPassword) => {
	return bcrypt.compareSync(password, hashPassword);
};

// capitalize function to capitalize first letter of a string
const capitalize = (s) => {
	return s[0].toUpperCase() + s.slice(1);
};

UserSchema.virtual('fullName').get(function() {
	return capitalize(this.firstName) + ' ' + capitalize(this.lastName);
});

UserSchema.methods.generateJWT = function() {
	return jwt.sign(
		{
			id: this._id,
			email: this.email,
			userName: this.userName
		},
		'wow_app'
	);
};

UserSchema.set('toObject', { virtuals: true });
UserSchema.set('toJSON', { virtuals: true });

UserSchema.methods.output = function() {
	return {
		_id: this._id,
		userName: this.userName,
		firstName: this.firstName,
		lastName: this.lastName,
		fullName: this.fullName,
		email: this.email,
		countryCode: this.countryCode,
		phoneNumber: this.phoneNumber,
		fullPhoneNumber: this.fullPhoneNumber,
		rating: this.rating,
		profile_image: this.profile_image,
		token: this.generateJWT()
	};
};

const User = rider_conn.model('User', UserSchema);
module.exports = User;
