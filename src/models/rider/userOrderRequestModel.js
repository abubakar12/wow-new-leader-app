const { rider_conn } = require('../../config/DBConnections');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserOrderRequestsSchema = new Schema(
	{
		userId: {
			type: String,
			index: true,
			required: [ true, 'User ID is required' ]
		},
		timeLabel: {
			type: String,
			required: [ true, 'Time Label is required' ]
		},
		timeInSeconds: {
			type: Number,
			required: [ true, 'Time in Seconds is required' ]
		},
		distanceInKmLabel: {
			type: String,
			required: [ true, 'Distance In Km Label is required' ]
		},
		distanceInMeters: {
			type: Number,
			required: [ true, 'Distance In Metres is required' ]
		},
		pickupLocation: {
			type: {
				type: String,
				enum: [ 'Point' ],
				default: 'Point'
			},
			coordinates: {
				type: [ Number ],
				required: true
			}
		},
		destinationLocation: {
			type: {
				type: String,
				enum: [ 'Point' ],
				default: 'Point'
			},
			coordinates: {
				type: [ Number ],
				required: true
			}
		},
		estimatedFareStartRange: {
			type: Number,
			required: [ true, 'Estimated Fare Start Range is required' ]
		},
		estimatedFareEndRange: {
			type: Number,
			required: [ 'Estimated Fare Start Range is required' ]
		},
		estimatedFareRange: {
			type: String,
			required: [ true, 'Estimated Fare Range is required' ]
		},
		currency: {
			type: String,
			enum: [ 'AED' ],
			default: 'AED',
			required: [ true, 'Currency is required' ]
		},
		driverId: {
			type: String,
			index: true
		},
		sendTime: Date,
		acceptTime: Date,
		rejectTime: Date,
		arrivalTime: Date,
		startRideTime: Date,
		endRideTime: Date,
		status: {
			type: String,
			enum: [ 'pending', 'accepted', 'rejected', 'driver arrived', 'start ride', 'completed' ],
			default: 'pending'
		},
		paymentStatus: {
			type: String,
			enum: [ 'pending', 'completed', 'milgyi' ],
			default: 'pending'
		}
	},
	{ timestamps: true }
);

UserOrderRequestsSchema.index({ pickupLocation: '2dsphere', destinationLocation: '2dsphere' });

const UserOrderRequest = rider_conn.model('UserOrderRequest', UserOrderRequestsSchema);
module.exports = UserOrderRequest;
