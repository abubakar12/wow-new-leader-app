const mongoose = require('mongoose');

const driverfilestemp_schema = mongoose.Schema({
	driverId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'signUpTemp'
	},
	driver_photo: {
		type: String
	},
	emirates_card_f: {
		type: String
	},
	expiry_date_emirates: {
		type: String,
	},
	emirates_card_b: {
		type: String
	},
	rta_card_f: {
		type: String
	},
	expiry_date_rta: {
		type: String,
	},
	rta_card_b: {
		type: String
	},
	rta_number:{
		type:String
	}
}, {
	timestamps: true,
	collection: 'DriverTempDocuments'
});

module.exports = mongoose.model('driverfiles_temp', driverfilestemp_schema);