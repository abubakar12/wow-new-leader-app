const mongoose = require('mongoose');
const vehiclefilesTemp_schema = mongoose.Schema({
	driverId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'signUpTemp'
	},
	categoryId: {
		type: String
	},
	vehicleId: {
		type: String
	},
	registrationNo: {
		type: String
	},
	year: {
		type: String
	},
	color: {
		type: String
	},
	expiryDate: {
		type: String,
	},
	vehicle_reg_f: {
		type: String
	},
	vehicle_reg_b: {
		type: String
	},
	categoryName: {
		type: String,
	},
	vehicleName: {
		type: String,
	}
}, {
	timestamps: true,
	collection: 'VehicleReviewDocument'
});
module.exports = mongoose.model('vehiclefiles_temp', vehiclefilesTemp_schema);