const {
	master_conn
} = require('../config/DBConnections');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const VehicleCategorySchema = new Schema({
	categoryName: {
		type: String,
		unique: true,
		required: [true, 'categoryName is required']
	},
	basicFare: {
		type: Number,
		required: [true, 'basicFare is required']
	},
	cancellationFare: {
		type: Number,
		required: [true, 'cancellationFare is required']
	},
	perKmFare: {
		type: Number,
		required: [true, 'perKmFare is required']
	},
	perMinuteFare: {
		type: Number,
		required: [true, 'perMinuteFare is required']
	},
	waitingFare: {
		type: Number,
		required: [true, 'waitingFare is required']
	},
	serviceFare: {
		type: Number,
		required: [true, 'serviceFare is required']
	},
	minimumFare: {
		type: Number,
		required: [true, 'minimumFare is required']
	},
	currencyType: {
		type: String,
		enum: ['AED'],
		default: 'AED',
		required: [true, 'Currency is required']
	},
	vehicles: [{
		name: {
			type: String,
			uppercase: true
		},
		image: String
	}],
	createdDate: {
		type: Date,
		default: Date.now
	}
}, {
	timestamps: true,
	collection: 'DriversVehicleCategory'
});
const VehicleCategory = master_conn.model('VehicleCategory', VehicleCategorySchema, 'vehicle_categories');
module.exports = VehicleCategory;