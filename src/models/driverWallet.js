const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const walletSchema = mongoose.Schema({
    driverId: {
        type: String,
        required: true
    },
    wallet: {
        type: String,
        required: true,
        default: null
    },
    outstandingBalance: {
        type: String,
        required: true,
        default: null
    },
    currency: {
        type: String
    },
    countryId:String,
    cityId:String
}, { timestamps: true,
    collection: 'DriverWallets' });
walletSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('driverWallet', walletSchema);