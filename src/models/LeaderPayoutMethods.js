const mongoose = require('mongoose');
const payoutInfo = mongoose.Schema({
         payoutType: String,
         address: String,
         accountHolderName: String,
         bankAccountNumber: String,
         IBAN: String,
         bankName: String
})

const payoutSchema = mongoose.Schema({
         driverId: mongoose.Schema.Types.ObjectId,
         payoutMethods: {
                  type: payoutInfo,
                  required: true
         },
         status:{
                  type:String,
                  enum:['pending','approve'],
                  default:'pending'
         }
},{
         timestamps:true,
         collection: 'DriverPayoutMethods'
});

module.exports = mongoose.model('PayoutMethods', payoutSchema, 'PayoutMethods');