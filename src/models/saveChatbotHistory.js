const mongoose = require('mongoose');
const history = mongoose.Schema({
         senderId: String,
         receiverId: String,
         body:String
})
const chatbotHistory = mongoose.Schema({
         driverId: String,
         History:{type: [history]}
}, {
         timestamps: true,
         collection: 'DriverChatBot'
});
module.exports = mongoose.model('chatbotHistory', chatbotHistory, 'ChatbotHistory');