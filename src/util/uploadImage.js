const allCredential = require('../config/allCredential');
const AWS = require('aws-sdk');

module.exports =async (driverId,imageData,imageType,s3Folder,imageSide)=>
{
    var folder;
    AWS.config.update({
        accessKeyId: allCredential.accessKeyId,
        secretAccessKey: allCredential.secretAccessKey,
        region: 'ap-southeast-1'
    });
    var cloudBase = new AWS.S3({
        params: {
            Bucket: 'wow-leader-app',
            ACL: 'public-read'
        }

    });
    switch(s3Folder){
        case "driverPhoto":
            folder='profileImg/'
            break;
        case "vehicle":
            folder='vehiclesDocs/'
            break;
        case "driverCards":
            folder='driversDocs/'
            break;

    }
    
    const docs = await cloudBase.upload({
        Key: (`${folder}_${driverId}_${folder.split('/')[0]}_${imageSide}.${imageType}` ),
        Body: imageData,
        ContentEncoding: 'base64', // required
        ContentType: `${imageType}`
    }).promise();

    console.log(docs);
    return docs;

}

