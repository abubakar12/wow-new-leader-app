const Pusher = require('pusher');
const credentials = require('../config/pusherCredentials');

const pusher = new Pusher({
  appId: credentials.pusherAppId,
  key: credentials.pusherPublicKey,
  secret: credentials.pusherSecretKey,
  cluster: 'ap2',
  useTLS: true
});

module.exports = pusher;