const allCredential = require('../config/allCredential');
const AWS = require('aws-sdk');
module.exports= AWS.config.update({
    accessKeyId: allCredential.accessKeyId,
    secretAccessKey: allCredential.secretAccessKey,
    region: 'ap-southeast-1'
});