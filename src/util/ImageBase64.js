const decodeBase64Image = dataString => {
    let response = {};
    const base64Data = new Buffer.from(dataString.replace(/^data:image\/\w+;base64,/, ''), 'base64');
    const type = dataString.split(';')[0].split('/')[1];
    response.type = type;
    response.data = base64Data;
    return response;
}
module.exports = { decodeBase64Image };