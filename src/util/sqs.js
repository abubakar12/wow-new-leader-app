const aws = require('aws-sdk');
const creds = require('../config/allCredential');
    aws.config.update({
    accessKeyId: creds.accessKeyId,
    secretAccessKey: creds.secretAccessKey,
    region: 'ap-southeast-1'
});
var sqs = new aws.SQS({apiVersion: '2012-11-05'});
module.exports= sqs;

