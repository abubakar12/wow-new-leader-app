const fs = require('fs');
const roles = {
    SERVICE: "112222",
    ADMIN: "112233",
    DRIVER: "112244",
    CUSTOMER: "112255"
  };
  const privateKey = fs.readFileSync("./private.key", "utf8");
const publicKey = fs.readFileSync("./public.key", "utf8");
  module.exports = {
    roles,
  privateKey,
  publicKey
  };