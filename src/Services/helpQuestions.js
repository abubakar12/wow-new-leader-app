const helpQuestions = require('../models/questionnaire');
const saveHistory = require('../models/saveChatbotHistory');
const moment = require('moment');
module.exports = class questionnaires {
        async addQuestionsForHelp(question, link) {
                let newQuestion = new helpQuestions({
                        question: question,
                        link: link
                });
                let saveQuestion = await newQuestion.save();
                return saveQuestion;
        }
        async getAllQuestions() {
                let quests = await helpQuestions.find({}).lean(true);
                return quests
        }
        async saveChatbotHistory(driverId, senderId, receiverId, msg) {
                let checkHistory = await saveHistory.findOne({ driverId: driverId, createdAt: { $gte: moment().startOf('D'), $lt: moment().endOf('D') } });
                if (checkHistory) {
                        checkHistory.History.push({
                                senderId: senderId,
                                receiverId: receiverId,
                                body: msg
                        });
                        await checkHistory.save();
                }
                else if (!checkHistory) {
                        let save = new saveHistory({
                                driverId: senderId,
                                History: {
                                        senderId: senderId,
                                        receiverId: receiverId,
                                        body: msg
                                }
                        });
                        await save.save();
                }
        }
        async getChatbotHistory(driverId) {
                let history = await saveHistory.findOne({ driverId }).lean(true);
                return history
        }
}