const DriverLocation = require('../models/driverLocations');
const orderHistory = require('../models/orderHistory');
const driverSignUp = require('../models/AuthN');
const driverDocs = require('../models/AuthNDriver');
const vehicleDocs = require('../models/AuthNVehicle');
const moment = require('moment');
const driverReview = require('../models/driverReviews');
const driverOderResponse = require('../models/driverOrderResponse');
const vehicleCategories = require('../models/vehicleCategory');
const loginLogs = require('../models/driverLoginsLogs');
const rideOrders = require('../models/rider/rideOrders');
const driverWallet = require('../models/driverWallet');
const statusLogs = require('../models/driverLoginsLogs');
const axios = require('axios')
var CryptoJS = require("crypto-js");
var secret = 'Qw4hd$$Again'
var request = require('request')
// const decodeImage = require('../util/decodeImage');
// const imageUpload = require('../util/uploadImage');

module.exports = class Location {
    async updateLocation(driverId, bateryStatus, newLatitude, newLongitude) {

        const driverFound = await DriverLocation.findOne({ driverId: driverId });
        if (driverFound) {
            let updatedLocation = await DriverLocation.findOneAndUpdate({ driverId: driverId }, {
                batteryStatus: bateryStatus,
                'location.coordinates.0': newLongitude,
                'location.coordinates.1': newLatitude
            },
                { new: true });
            if (updatedLocation) {
                return updatedLocation;
            }

        }
        else {
            let newDriverLocation = new DriverLocation({
                driverId: driverId,
                batteryStatus: bateryStatus,
                location: {
                    type: 'Point',
                    coordinates: [newLongitude, newLatitude]
                }
            });
            //   console.log(newDriverLocation);
            let driver = await newDriverLocation.save();
            if (driver) {
                return driver;
            }


        }

    }
    async driverBasicInfo(driverId) {
        let driverInfo = await driverSignUp.findById({ _id: driverId });
        if (driverInfo) {
            let driverStatuses = await DriverLocation.findOne({ driverId: driverId });
            if (driverStatuses) {
                let vehicleDetail = await vehicleDocs.findOne({ driverId: driverId }, { color: 1, year: 1, registrationNo: 1, vehicleName: 1, categoryName: 1 });

                return {
                    driverId: driverInfo._id,
                    fullName: `${driverInfo.first_name} ${driverInfo.last_name}`,
                    email: driverInfo.email,
                    licenseNumber:driverInfo.licenseNumber,
                    fullPhoneNumber: driverInfo.fullPhoneNumber,
                    deviceToken: driverInfo.deviceToken,
                    rating: driverInfo.rating,
                    profileImage: driverInfo.profileImage,
                    batteryStatus: driverStatuses.batteryStatus,
                    location: driverStatuses.location.coordinates,
                    vehicleDetail
                }

            }


        }
    }
    async findNearDriversByLocation(lat, long) {
        try {

            let longitude = parseFloat(long);
            let latitude = parseFloat(lat);
            const driver = new DriverLocation({
                location: {
                    type: 'Point',
                    coordinates: [longitude, latitude]
                }
            });
            let drivers = await driver.findNearByDrivers();
            if (drivers) {
                return drivers
            }
            else if (drivers.errors) {
                throw errors
            }
        }
        catch (err) {
            throw err;
        }
    }
    async findDriversByStatus(status, lat, long) {
        try {

            let longitude = parseFloat(long);
            let latitude = parseFloat(lat);
            const driver = new DriverLocation({
                location: {
                    type: 'Point',
                    coordinates: [longitude, latitude]
                }
            });
            let Status = await driver.findNearByDriversByStatus(status);
            if (Status) {
                return Status
            }
            else if (Status.errors) {
                throw Status.errors
            }

        }
        catch (err) {
            throw err;
        }
    }
    async findDriverByRadius(status, radius, lat, long) {
        try {
            let longitude = parseFloat(long);
            let latitude = parseFloat(lat);
            const driver = new DriverLocation({
                location: {
                    type: 'Point',
                    coordinates: [longitude, latitude]
                }
            });
            let driverInRadius = await driver.findNearByDriversByRadius(status, radius);
            if (driverInRadius) {
                return driverInRadius
            }
            else if (driverInRadius.errors) {
                throw errors;

            }
        } catch (err) {
            throw err;
        }
    }
    async findDriverByRadiusIds(status, radius, lat, long) {
        try {
            let longitude = parseFloat(long);
            let latitude = parseFloat(lat);
            const driver = new DriverLocation({
                location: {
                    type: 'Point',
                    coordinates: [longitude, latitude]
                }
            });
            let driverInRadius = await driver.findNearByDriversByRadius(status, radius);
            let result = driverInRadius.map(element => element.driverId);
            //   console.log(result);
            return result

        } catch (err) {
            throw err;
        }
    }
    async countDriverByRadius(status, radius, lat, long) {
        try {
            let longitude = parseFloat(long);
            let latitude = parseFloat(lat);
            const driver = new DriverLocation({
                location: {
                    type: 'Point',
                    coordinates: [longitude, latitude]
                }
            });
            let driverInRadius = await driver.countNearByDriversByRadius(status, radius);
            //  console.log(driverInRadius)
            return driverInRadius

        } catch (err) {
            throw err;
        }
    }
    async rideHistory(driverId, startDate, endDate) {
        if ((startDate == '' && endDate == '') || (startDate == null && endDate == null)) {
            let orders = await rideOrders.find({ driverId: driverId, rideStatus: 'rideCompleted' });
            if (orders) {
                return orders
            }
        }
        else if (startDate != null && endDate != null) {
            let startDa = startDate.split('/')[0];
            let startM = startDate.split('/')[1];
            let startY = startDate.split('/')[2];
            let startDateFinal = `${startY}-${startM}-${startDa}`;
            let endDa = endDate.split('/')[0];
            let endM = endDate.split('/')[1];
            let endY = endDate.split('/')[2];
            let endDateFinal = `${endY}-${endM}-${endDa}`;
            let weekRide = await orderHistory.aggregate([{
                $addFields: { "creationDate": { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } } }
            },
            {
                $match: { driverId: driverId, creationDate: { $gte: startDateFinal, $lte: endDateFinal } }
            }]);
            if (weekRide) {
                return weekRide
            }
        }
    }
    async driverDocuments(driverId) {
        let driver = await driverSignUp.findOne({ _id: driverId });
        let docs = await driverDocs.findOne({ driverId: driverId });
        if (driver != '' && docs != '') {
            return {
                emiratesID: driver.emiratesID,
                emirates_card_f: docs.emirates_card_f,
                expiry_date_emirates: docs.expiry_date_emirates,
                emirates_card_b: docs.emirates_card_b,
                rta_card_f: docs.rta_card_f,
                expiry_date_rta: docs.expiry_date_rta,
                rta_card_b: docs.rta_card_b,
                rta_number: docs.rta_number
            }
        }

    }
    async vehicleDocuments(driverId) {
        let docs = await vehicleDocs.findOne({ driverId: driverId });
        if (docs != '') {
            return {
                registrationNo: docs.registrationNo,
                vehicle_reg_f: docs.vehicle_reg_f,
                expiryDate: docs.expiryDate,
                vehicle_reg_b: docs.vehicle_reg_b,
            }
        }

    }
    async rideHistoryWeekly(driverId, startDate, endDate) {
        let startD = new Date();
        let endD = new Date();
        let ary;
        let ary2 = [];
        let arrayFinal = [0, 0, 0, 0, 0, 0, 0];
        // console.log(startDate);


        let startDa = startDate.split('/')[0];
        let startM = startDate.split('/')[1];
        let startY = startDate.split('/')[2];
        let startDateFinal = `${startY}-${startM}-${startDa}`;
        // console.log(startDateFinal)
        let endDa = endDate.split('/')[0];
        let endM = endDate.split('/')[1];
        let endY = endDate.split('/')[2];
        let endDateFinal = `${endY}-${endM}-${endDa}`;
        var weekTip = 0, weekBonus = 0, weekRideEarning = 0, weekTollG = 0, weekParking = 0, weekCancel = 0, weekR = 0;
        var totalTip = 0, totalBonus = 0, totalRideEarning = 0, totalTollG = 0, totalParking = 0, totalCancel = 0, totalDistance = 0, totalRides = 0;
        let weekRide = await orderHistory.aggregate([{
            $addFields: { "creationDate": { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } } }
        },
        {
            $match: { driverId: driverId, creationDate: { $gte: startDateFinal, $lte: endDateFinal } }
        }]);
        let dailyRecord = await orderHistory.aggregate([{
            $addFields: { "creationDate": { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } } }
        },
        {
            $match: { driverId: driverId, creationDate: { $gte: startDateFinal, $lte: endDateFinal } }
        }, { $group: { _id: { driverId: '$driverId', date: '$creationDate' }, entries: { $push: { amount: '$amount', } } } },
        { $group: { _id: '$_id.driverId', dates: { $push: { date: '$_id.date', amount: { '$sum': '$entries.amount' } } } } }
        ]);
        let totalRecord = await orderHistory.find({ driverId: driverId });
        startD.setUTCHours(0, 0, 0, 0)
        startD.setFullYear(startDate.split('/')[2]);
        startD.setMonth(startDate.split('/')[1] - 1);
        startD.setUTCDate(startDate.split('/')[0])
        endD.setUTCHours(0, 0, 0, 0)
        endD.setFullYear(endDate.split('/')[2]);
        endD.setMonth(endDate.split('/')[1] - 1);
        endD.setUTCDate(endDate.split('/')[0]);
        var dateArray = [];
        var currentDate = moment(startD);
        var stopDate = moment(endD);
        while (currentDate <= stopDate) {
            dateArray.push(moment(currentDate).format('YYYY-MM-DD'))
            currentDate = moment(currentDate).add(1, 'days');
        }
        //  console.log(dateArray)
        dailyRecord.map(item => { ary = item; });
        if (ary != null) {
            ary.dates.forEach(t => { ary2.push(t) })
            ary2.map(item => {
                for (var i = 0; i < 7; i++) {
                    if (dateArray[i] == item.date) {
                        arrayFinal[i] = item.amount
                    }
                }
            });
        }
        if (weekRide) {

            weekRide.forEach(items => {
                weekTip = weekTip + items.tip;
                weekBonus = weekBonus + items.bonus;
                weekRideEarning = weekRideEarning + items.amount;
                weekTollG = weekTollG + items.tollGate;
                weekParking = weekParking + items.parking;
                weekCancel = weekCancel + items.cancelRide;
                weekR = weekR + 1;
            });
            // console.log(weekR);
        }
        if (totalRecord) {
            totalRecord.forEach(items => {
                totalTip = totalTip + items.tip;
                totalBonus = totalBonus + items.bonus;
                totalRideEarning = totalRideEarning + items.amount;
                totalTollG = totalTollG + items.tollGate;
                totalParking = totalParking + items.parking;
                totalCancel = totalCancel + items.cancelRide;
                totalDistance = totalDistance + items.distance;
                totalRides = totalRides + 1;
            });
        }

        return {
            walletBalance: `AED ${weekTip + weekBonus + weekRideEarning + weekTollG + weekParking + weekCancel}`,
            rideEarning: `AED ${weekRideEarning}`,
            tollGate: `AED ${weekTollG}`,
            parking: `AED ${weekParking}`,
            cancelRide: `AED ${weekCancel}`,
            tip: `AED ${weekTip}`,
            bonus: `AED ${weekBonus}`,
            totalRides: totalRides,
            totalDistance: `${totalDistance} Km`,
            totalBalance: `${totalTip + totalBonus + totalRideEarning + totalTollG + totalParking + totalCancel} AED`,
            dailyAmount: arrayFinal
        }

    }
    //driver side orders Api's
    async changeOrderStatusAccept(driverId, orderId) {
        let date = new Date();
        let saveStatus = await driverOderResponse.findOneAndUpdate({ driverId: driverId, orderId: orderId }, { status: 'accepted', acceptTime: new Date(), isCancel: false }, { new: true });
        if (saveStatus) {
            let engagedStatus = await DriverLocation.findOneAndUpdate({ driverId: driverId }, { status: 'engaged' }, { new: true });
            if (engagedStatus) {
                return saveStatus
            }
        }

    }
    async changeOrderStatusReject(driverId, orderId) {
        let saveStatus = await driverOderResponse.findOneAndUpdate({ driverId: driverId, orderId: orderId }, { status: 'rejected', rejectTime: new Date(), isCancel: false }, { new: true });
        if (saveStatus) {
            let onStatus = await DriverLocation.findOneAndUpdate({ driverId: driverId }, { status: 'on' }, { new: true });
            if (onStatus) {
                return saveStatus
            }
        }

    }
    async changeOrderStatusCancel(driverId, orderId) {


        let saveStatus = await driverOderResponse.findOneAndUpdate({ driverId: driverId, orderId: orderId }, { isCancel: true, cancelBy: 'driver' }, { new: true });
        if (saveStatus) {
            return saveStatus;
        }

    }


    //Customer side Order Api
    async getDriverLocation(driverId) {
        let driverData = await DriverLocation.findOneAndUpdate({ driverId: driverId }, { status: 'busy' }, { new: true });
        if (driverData) {
            return driverData;
        }
    }
    async driverOrderSave(driverId, orderId, batteryStatus, driverLat, driverLong, requestSendTime) {
        let date = new Date(requestSendTime);
        const saveOrd = new driverOderResponse({
            driverId: driverId,
            orderId: orderId,
            driverBatteryStatus: batteryStatus,
            driverLocation: { type: "Point", coordinates: [driverLong, driverLat] },
            orderSaveTime: date
        });
        let saveOrder = await saveOrd.save();
        // console.log(saveOrder);
        if (saveOrder) {
            let busyStatus = await DriverLocation.findOneAndUpdate({ driverId: driverId }, { status: 'busy' }, { new: true });
            if (busyStatus)
                return { saveOrder, busyStatus };

        }

    }
    async timePassedStatus(driverId, orderId) {
        let changeStatus = await driverOderResponse.findOneAndUpdate({ driverId: driverId, orderId: orderId }, { status: 'timePassed' }, { new: true });
        if (changeStatus) {
            let driverStatus = await DriverLocation.findOneAndUpdate({ driverId: driverId }, { status: 'on' }, { new: true });
            if (driverStatus) {
                return changeStatus.status
            }
        }
    }
    async changeStatusCancelCus(driverId, orderId) {
        let saveStatus = await driverOderResponse.findOneAndUpdate({ driverId: driverId, orderId: orderId }, { isCancel: true, cancelBy: 'customer' }, { new: true });
        if (saveStatus) {
            return saveStatus;
        }

    }
    /////////
    async showStatus(driverId, orderId) {
        let status = await driverOderResponse.findOne({ driverId: driverId, orderId: orderId });
        //   console.log(status.status);
        if (status) {
            return status.status;
        }

    }
    async getOrdersaveTime(driverId, orderId) {
        let time = await driverOderResponse.findOne({ driverId: driverId, orderId: orderId }, { orderSaveTime: 1 });
        if (time) {
            return time;
        }
    }

    async rideWeekTest(driverId, startDate, endDate) {
        //  console.log(startDate)
        let startD = startDate.split('/')[0];
        let startM = startDate.split('/')[1];
        let startY = startDate.split('/')[2];
        let startDateFinal = `${startY}-${startM}-${startD}`;
        // console.log(startDateFinal)
        let endD = endDate.split('/')[0];
        let endM = endDate.split('/')[1];
        let endY = endDate.split('/')[2];
        let endDateFinal = `${endY}-${endM}-${endD}`;
        //let dailyRecord = await orderHistory.aggregate().match({ driverId:driverId, date: {$gte: startDate,$lte: endDate}})
        // .group({_id: {driverId: '$driverId',date: '$date',},entries: {$push: {amount:'$amount',}}})
        // .group({_id: '$_id.driverId',dates: {$push: {date: '$_id.date',amount: {'$sum':'$entries.amount'}}}});
        let t = await orderHistory.aggregate([{
            $addFields: { "creationDate": { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } } }
        },
        {
            $match: { driverId: driverId, creationDate: { $gte: startDateFinal, $lte: endDateFinal } }
        }, { $group: { _id: { driverId: '$driverId', date: '$creationDate' }, entries: { $push: { amount: '$amount', } } } },
        { $group: { _id: '$_id.driverId', dates: { $push: { date: '$_id.date', amount: { '$sum': '$entries.amount' } } } } }
        ]);
        return t;
    }
    async saveDriverReview(driverId, customerProfile, customerName, customerRating, customerComments) {
        const review = new driverReview({
            driverId: driverId,
            customerProfile: customerProfile,
            customerName: customerName,
            customerRating: customerRating,
            customerComment: customerComments

        });
        let saveReview = await review.save();
        if (saveReview) {
            return saveReview;
        }
    }
    async showReview(driverId) {
        let reviews = await driverReview.find({ driverId: driverId });
        if (reviews) {
            return reviews;
        }
    }
    ////
    async showVehicleDetails(driverId) {
        const vehicle = await vehicleDocs.find({ driverId: driverId });
        
        let result = await this.processVehicleDetail(vehicle);
         
        return result;
    }

    async processVehicleDetail(vehicle) {
        let array = [];
        for (let index = 0; index < vehicle.length; index++) {
            let cate = await vehicleCategories.findOne({ _id: vehicle[index].categoryId });
            cate.vehicles.map((items) => {
                if (items._id == vehicle[index].vehicleId) {
                    let obj = {};
                    obj.image = items.image;
                    obj.vehicleId = vehicle[index].vehicleId;
                    obj.categoryId = vehicle[index].categoryId;
                    obj.registrationNo = vehicle[index].registrationNo;
                    obj.vehicleName = vehicle[index].vehicleName;
                    array.push(obj);
                    //  console.log(obj);

                }
            });
        }
        return array;
    }
    async getRideStatus(driverId) {
        let driverOrders = await driverOderResponse.findOne({ driverId: driverId }).sort({ _id: -1 });
        if (driverOrders.status == 'accepted' && driverOrders.isCancel == false) {
            return driverOrders
        }
        else {
            return 'status is rejected or cancel or pending'
        }
    }
    async getDToken(driverId) {
        let token = await driverSignUp.findById({ _id: driverId }, { deviceToken: 1 });
        if (token) {
            return token
        }
    }
    async updateDriverStatusAfterCancel(driverId) {
        let updateStatus = await DriverLocation.findOneAndUpdate({ driverId: driverId }, { status: 'on' }, { new: true });
        if (updateStatus) {
            return updateStatus.status;
        }
    }
    async statusOn(driverId, status) {
        let stat = await DriverLocation.findOneAndUpdate({ driverId: driverId }, { status: status }, { new: true });
        if (stat) {
            return stat;
        }
    }
    async updateRating(driverId, rating) {
        let driver = await driverSignUp.findById({ _id: driverId }, { rating: 1, _id: 0 });
        if (!driver) { return }
        let Rating = parseFloat(parseFloat(driver.rating) + rating) / 2;
        let updateRating = await driverSignUp.findByIdAndUpdate({ _id: driverId }, { rating: Rating }, { new: true });
        return updateRating.rating;
    }
    async driverLocationData(driverId) {
        let data = await DriverLocation.findOne({ driverId: driverId });
        if (data) {
            return data;
        }
    }
    async getOnlineTime(driverId) {
        var duration;
        let endIndex;
        let date = moment(new Date());
        let logs = await loginLogs.find({ driverId, createdAt: { $gt: date.startOf('day').toISOString(), $lt: date.endOf('day').toISOString() } });
        //return logs;
        //    if(logs[0].startTime!=null){
        //        for(let i=0 ;i<logs.length;i++){
        //            if(logs[i].endTime!=null){
        //                duration=moment.duration(moment(logs[i].endTime).diff(moment(logs[0])));
        //                endIndex=i;
        //               continue;

        //            }
        //            else {
        //                console.log(i,"end",endIndex);
        //            }
        //        }
        //    }
        //    else if(logs[0].endTime!=null){

        //    }

        //     var logsArrayLength = logs.length;

        //     if(logs.length==0)
        //     return {time:`00:00`}
        //  else { 
        //      let endCheck=logs[logsArrayLength-1].startTime!=null;
        //     if(logs[0].startTime!=null){
        //         startTime = logs[0].startTime;
        //         for(var i=0;i<logs.length;i++){
        //             if(logs[i].startTime!=null){
        //                 if(startTime==null){
        //                     startTime=new Date(logs[i].startTime);}
        //                 else if(startTime!=null){
        //                  countMinutes=countMinutes+( Math.floor(logs[i].startTime-startTime)/1000)/60;
        //                  startTime=new Date(logs[i].startTime); 
        //             }
        //             }
        //             else if(logs[i].endTime!=null){

        //                 countMinutes=countMinutes+( Math.floor(new Date(logs[i].endTime)-startTime)/1000)/60;
        //                 startTime=null; 
        //             }
        //         }
        //         if(endCheck){
        //             countMinutes=countMinutes+( Math.floor(new Date()-startTime)/1000)/60; 
        //         }
        //         var hours = Math.floor(countMinutes / 60);          
        //         var minutes = Math.floor(countMinutes % 60);

        //     }
        //     else if(logs[0].endTime!=null){
        //         countMinutes= countMinutes+((Math.floor(new Date().setHours(0,0,0,0))-new Date(logs[0].endTime)/1000)/60)
        //         for(var i=1;i<logs.length;i++){
        //             if(logs[i].startTime!=null){
        //                 if(startTime==null){
        //                     startTime=new Date(logs[i].startTime);}
        //                 else if(startTime!=null)
        //                { countMinutes=countMinutes+( Math.floor(logs[i].startTime-startTime)/1000)/60;
        //                 startTime=new Date(logs[i].startTime);
        //             }
        //             }
        //             else if(logs[i].endTime!=null){
        //                 countMinutes=countMinutes+( Math.floor(new Date(logs[i].endTime)-startTime)/1000)/60;
        //                 startTime=null;
        //             }
        //         }
        //         if(endCheck){
        //             countMinutes=countMinutes+( Math.floor(new Date()-startTime)/1000)/60;
        //             console.log("Start Minutes Last",countMinutes);
        //         }
        //         var hours = Math.floor(countMinutes / 60);          
        //         var minutes = Math.floor(countMinutes % 60);
        //     }}
        return { time: `11:1` }
    }
    async cancelOrderByCustomer(driverId, orderId) {
        let cancelOrder = await driverOderResponse.findOneAndUpdate({ driverId: driverId, orderId: orderId }, { status: 'cancel' }, { new: true });
        if (cancelOrder) {
            return cancelOrder;
        }
    }
    async getTodayDriverAnalytics(driverId) {

        return await axios.get('http://18.138.89.166:9002/api/v1/transactionHistory/getLeaderTodayRidesAndEarning', { data: { driverId: driverId } });

    }
    async searchContactsForDriverInvite(contacts) {
        let inviteContacts = [];
        var i = 0;
        for (i = 0; i < contacts.length; i++) {
            let numbe = contacts[i].number;
            if (numbe.length <= 6) {
                inviteContacts.push(contacts[i]);
            }
            else {
                let withoutCharacterNumber = numbe.replace(/[^\d\+]/g, "");
                let finalContact = withoutCharacterNumber.substr(4);
                let search = await driverSignUp.find({ fullPhoneNumber: { $regex: finalContact } });
                if (search == '') {
                    inviteContacts.push(contacts[i]);
                }
            }

        }
        return inviteContacts
    }
    async driverWallet(params) {

        let walletFound = await driverWallet.findOne({ driverId: params.driverId });
        if (walletFound) {

            var bytes = CryptoJS.AES.decrypt(walletFound.wallet.toString(), secret);
            var walletDecrypt = bytes.toString(CryptoJS.enc.Utf8);
            let wallet = parseFloat(walletDecrypt) + parseFloat(params.wallet);
            var walletAmount = CryptoJS.AES.encrypt(wallet.toString(), secret).toString();
            var outstandingAmount = CryptoJS.AES.encrypt(params.outstandingAmount, secret).toString();
            let updateWallet = await driverWallet.findOneAndUpdate({ driverId: params.driverId }, { wallet: walletAmount, outstandingBalance: outstandingAmount ,countryId:params.countryId,cityId:params.cityId}, { new: true });
            if (updateWallet) {
                return updateWallet
            }
        }
        else if (!walletFound) {
            var walletAmount = CryptoJS.AES.encrypt(params.wallet, secret).toString();
            var outstandingAmount = CryptoJS.AES.encrypt(params.outstandingAmount, secret).toString();
            let saveWallet = new driverWallet({
                driverId: params.driverId,
                wallet: walletAmount,
                outstandingBalance: outstandingAmount,
                currency: params.currency
            });
            let walletSaved = await saveWallet.save();
            if (walletSaved) {
                return walletSaved
            }
        }
    }
    async getCountryCode() {

        return new Promise(function (resolve, reject) {
            request('http://18.138.196.66:4000/api/v1/master/country/getAllActiveCountries/1/100', function (error, res, body) {
                if (!error && res.statusCode == 200) {
                    resolve(body);
                } else {
                    reject(error);
                }
            });
        });


    }
    async getCodeCountry(countryCode) {
        let data = await this.getCountryCode();
        let code;
        let d = JSON.parse(data);
        let dataArray = d.response.docs;
        for (let i = 0; i < dataArray.length; i++) {
            if (dataArray[i].phone === countryCode) {
                code = dataArray[i]

            }

        }
        return code;


    }


    async getTotalOnlineTime(driverId) {

        let loginLogs = await statusLogs.find({ driverId });
        //return loginLogs
        return { time: `${hours}:${minutes}` }
    }
    async getTotalRides(driverId) {
        let rides = await rideOrders.find({ driverId: driverId })

    }
    async getWallet(driverId) {
        let walletFound = await driverWallet.findOne({ driverId: driverId });
        if (walletFound) {
            var bytes = CryptoJS.AES.decrypt(walletFound.wallet.toString(), secret);
            var walletDecrypt = bytes.toString(CryptoJS.enc.Utf8);
            return { wallet: walletDecrypt }
        }
        else if (!walletFound) {
            return { wallet: '0' }
        }
    }
    async getDriverDetail(driverId) {
        let detail = await driverSignUp.findById({ _id: driverId });
        return detail;
    }
    async getAllDriverWallet(page, limit) {
        const myCustomLabels = {
            docs: 'data',
        };

        var options = {
            page: page,
            limit: limit,
            customLabels: myCustomLabels,
            lean: true
        }
        let wallet = await driverWallet.paginate({}, options);
        let docsArray = wallet.data;
        for (let i = 0; i < docsArray.length; ++i) {
            var bytes = CryptoJS.AES.decrypt(docsArray[i].wallet.toString(), secret);
            var walletDecrypt = bytes.toString(CryptoJS.enc.Utf8);
            var bytes1 = CryptoJS.AES.decrypt(docsArray[i].outstandingBalance.toString(), secret);
            var outstandingDecrypt = bytes1.toString(CryptoJS.enc.Utf8);
            wallet.data[i].wallet = parseFloat(walletDecrypt)
            wallet.data[i].outstandingBalance = parseFloat(outstandingDecrypt)
        }
        return wallet
    }
    async getDriverContacts() {
        let contacts = await driverSignUp.find({}, { first_name: 1, last_name: 1, fullPhoneNumber: 1 });
        let obj = [];
        contacts.forEach(items => {
            obj.push({
                fullName: `${items.first_name} ${items.last_name}`,
                phoneNumber: items.fullPhoneNumber
            });
        })
        return obj
    }
    async saveLeaderPreferences(driverId, baggage, passenger, childSeat, rideAssistance, drinkingWater, temperature) {
        let driver = await driverSignUp.findByIdAndUpdate({ _id: driverId }, {
            'preferences.baggage': baggage,
            'preferences.passenger': passenger,
            'preferences.childSeat': childSeat,
            'preferences.rideAssistance': rideAssistance,
            'preferences.drinkingWater': drinkingWater,
            'preferences.temperature': temperature,

        }, { new: true });
        return driver;
    }
    async showLeaderSelectedPreferences(driverId){
        return await driverSignUp.findById({_id:driverId},{preferences:1}).lean(true);
    }
  

}


