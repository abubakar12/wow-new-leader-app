const payoutMethod = require('../models/LeaderPayoutMethods');
module.exports = class payoutMethods {

         async addNewPayoutMethods(params) {
                  let findMethod = await payoutMethod.findOne({ driverId: params.driverId });
                  if (!findMethod) {
                           let addNew = new payoutMethod({
                                    driverId: params.driverId,
                                    payoutMethods: {
                                             payoutType: params.payoutType,
                                             address: params.address,
                                             accountHolderName: params.accountHolderName,
                                             bankAccountNumber: params.bankAccountNumber,
                                             IBAN: params.IBAN,
                                             bankName: params.bankName
                                    }
                           });
                           let save = await addNew.save();
                           return save
                  }
                  else {
                           let err = new Error('Payout method already exist')
                           err.statusCode = 400
                           throw err;
                  }
         }

         async updatePayoutMethod(params) {
            let findMethod = await payoutMethod.findOneAndUpdate({ driverId: params.driverId },{
                  driverId: params.driverId,
                  payoutMethods: {
                           payoutType: params.payoutType,
                           address: params.address,
                           accountHolderName: params.accountHolderName,
                           bankAccountNumber: params.bankAccountNumber,
                           IBAN: params.IBAN,
                           bankName: params.bankName
                  }
         },{new:true});
            if (!findMethod) {
                  let error = new Error('Payout method not updated');
                  error.statusCode = 400
                  throw error;
            }
           
   }

         async getPayoutMethods() {
                  let method = await payoutMethod.find({}).lean(true);
                  console.log(method)
                  if (method == '') {
                           let error = new Error('Payout method not found');
                           error.statusCode = 400
                           throw error;
                  }
                  else {
                           return method
                  }
         }
         async getLeaderPayout(driverId) {
               return await payoutMethod.findOne({ driverId ,status:'approve'}).lean(true);
                 
         }
}