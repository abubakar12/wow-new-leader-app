const adminDAL = require('../DAL/LeaderDAL');
var AdminDAL = new adminDAL();
const DriverOrderResponse = require('../models/driverOrderResponse');
module.exports = class MasterAdmin {

    async getPendingLeadersByAdmin(countryId, cityId, startDate, endDate, page, limit) {
        // let picked = {_id:0};
        let pendingLeaders = await AdminDAL.getPendingLeaderByFilters(countryId, cityId, startDate, endDate, page, limit);
        return pendingLeaders;
    }
    async getApprovedLeadersByAdmin(countryId, cityId, page, limit, startDate, endDate) {
        //const picked = { first_name: 1, last_name: 1, email: 1, profileImage: 1, pinCode: 1, blockedState: 1, fullPhoneNumber: 1, rating: 1, createdAt: 1 }
        // let picked = {_id:0};
        let getApproved = await AdminDAL.getApprovedLeaderByFilters(countryId, cityId, page, limit, startDate, endDate);
        return getApproved;
    }
    async getBlockedLeadersByAdmin(countryId, cityId, page, limit, startDate, endDate) {
        //const picked = { first_name: 1, last_name: 1, email: 1, profileImage: 1, pinCode: 1, blockedState: 1, fullPhoneNumber: 1, rating: 1, createdAt: 1 }
        // let picked = {_id:0};
        let getBlocked = await AdminDAL.getBlockedLeadersByFilters(countryId, cityId, page, limit, startDate, endDate)
        for (let i = 0; i < getBlocked.docs.length; i++) {
            let reason = await AdminDAL.getBlockedReason(getBlocked.docs[i]._id);
            getBlocked.docs[i].blockedReason = reason.blockedReason;
            delete getBlocked.docs[i].id
        }
        return getBlocked;
    }
    async getDisApprovedLeadersByAdmin(countryId, cityId, startDate, endDate, page, limit) {
        let getDisApproved = await AdminDAL.getDisApprovedLeadersByFilters(countryId, cityId, startDate, endDate, page, limit)
        for (let i = 0; i < getDisApproved.docs.length; i++) {
            let reason = await AdminDAL.getDisApproveReason(getDisApproved.docs[i]._id);
            getDisApproved.docs[i].disApprovalReason = reason.disApprovalReason;
            delete getDisApproved.docs[i].id;
        }
        return getDisApproved;
    }
    async getActiveLeadersByAdmin(countryId, cityId, startDate, endDate, page, limit) {
        var sixMonAgo = new Date(new Date().getFullYear(), new Date().getMonth() - 6, new Date().getDate());
        var myAggregate = DriverOrderResponse.aggregate();
        myAggregate.match({ 'createdAt': { $gte: sixMonAgo } }).group({ '_id': '$driverId' })
        const results = await DriverOrderResponse.aggregatePaginate(myAggregate, { page: page, limit: limit })
        let activeLeaders = results.data.map(key => { return key });
        let getActiveLeaders = await AdminDAL.getActiveLeadersByFilters(countryId, cityId, startDate, endDate, activeLeaders, page, limit)
        return getActiveLeaders;
    }
    async getInActiveLeadersByAdmin(countryId, cityId, startDate, endDate, page, limit) {
        var sixMonAgo = new Date(new Date().getFullYear(), new Date().getMonth() - 6, new Date().getDate());
        var myAggregate = DriverOrderResponse.aggregate();
        myAggregate.group({ _id: '$driverId', max: { $max: '$createdAt' } }).match({ max: { $lt: sixMonAgo } })
        let results = await DriverOrderResponse.aggregatePaginate(myAggregate, { page: page, limit: limit })
        console.log(results);
        let inActiveLeaders = results.data.map(key => { return key });
        let getInActiveLeaders = await AdminDAL.getActiveLeadersByFilters(countryId, cityId, startDate, endDate, inActiveLeaders, page, limit)
        return getInActiveLeaders;
    }
    async getAllLeaderDetailsByAdmin(driverId) {
        let getDetails = await AdminDAL.getAllLeaderDetailsFromTables(driverId)
        return getDetails;
    }
    async approveToBlockByAdmin(driverId, blockedReason) {
        let postDrivers = await AdminDAL.approveToBlock(driverId, blockedReason);
        return postDrivers;
    }
    async blockToApproveByAdmin(driverId) {
        let delApproveDrivers = await AdminDAL.blockToApprove(driverId);
        return delApproveDrivers;
    }
    async leaderApproveByAdmin(driverId) {
        let doApprove = await AdminDAL.pendingToApprove(driverId);
        return doApprove;
    }
    async disApproveToPendingByAdmin(driverId) {
        let delDisApproveDrivers = await AdminDAL.disApproveToPending(driverId);
        return delDisApproveDrivers;
    }
    async leaderDisApproveByAdmin(driverId, disApprovalReason) {
        let doDisApprove = AdminDAL.pendingToDisApprove(driverId, disApprovalReason);
        return doDisApprove;
    }
    async getOnlineLeaderByAdmin(countryId, cityId, startDate, endDate, page, limit) {
        return await AdminDAL.getOnlineDrivers(countryId, cityId, startDate, endDate, page, limit);
    }
    async getCarsCounts() {
        return await AdminDAL.getCarsCountForAdminMap();
    }
    async getSearchingCarsByAdmin(lat,long) {
        return await AdminDAL.getDriverSearchingCars(lat,long);
    }
     

}