const notification = require('../models/notifications');

module.exports = class notificationService {
         async saveNewNotification(driverId, title, body, type) {
                  let checkNotification = await notification.find({ driverId });
                  if (!checkNotification) {
                           let saveFirstOne = new notification({
                                    driverId: driverId,
                                    'notification.title': title,
                                    'notification.body': body,
                                    'notification.notificationType': type,
                                    'notification.heading': heading,
                           });
                           await saveFirstOne.save();
                  }
                  else {
                           let saveNewOne = new notification({
                                    driverId: driverId,
                                    'notification.title': title,
                                    'notification.body': body,
                                    'notification.notificationType': type
                           });
                           await saveNewOne.save();
                  }

         }
         async getLeaderNotifications(driverId) {
                  return await notification.find({ driverId }).lean(true);

         }
}