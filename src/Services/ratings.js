const axios = require('axios');
module.exports = class leaderRating {
         async getRating(driverId,page,limit) {
                  
                  let ratingData = await axios.get(`http://18.138.89.166:9005/api/v1/rider/rideRating/getDriverRating/${driverId}/${page}/${limit}`)
                  return ratingData.data;
         }
        
         async getCustomerProfile(customerId) {
                  
                  let data = await axios.post('http://18.138.89.166:9000/api/v1/rider/customer/findCustomerShortProfileById', { customerId });
                  return data.data.response;
         }
        
}