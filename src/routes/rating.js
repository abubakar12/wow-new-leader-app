const router = require('express').Router();
const { getDriverRating } = require('../controllers/ratingController');
router.get('/getDriverReviews/:driverId/:page/:limit', getDriverRating)

module.exports = router