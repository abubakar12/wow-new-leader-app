// AuthN Driver Controller
const { temporarySignUp, pendingSignUp, verifyPinCode, touchIdLogin, refreshDeviceToken, driverDetails, updateDriverInfo, logout, } = require('../controllers/AuthNDriver');

// For OTP AUTHN
const { otp_verification_start, otp_verification_check } = require('../controllers/AuthNOTP');

// All Categories Controller
const { getAllCategoryNames, getVehiclesByCategory, getVehicleByDriverId, getListOfDriverVehicles } = require('../controllers/vehicleCategoryController');

// For Driver Location Controllers
const { createUpdateDriverLocation, findNearByDrivers, changeDriverStatus } = require('../controllers/driverLocationController');

// For Driver Order Response Controllers
const { acceptOrRejectOrder, getDriverOrderResponseStatus, } = require('../controllers/driverOrderResponseController');

// AuthAdmin API's
const { disApprovedToPending, getAllApprovedLeaders, getAllBlockedLeaders, getAllPendingLeaders, getActiveLeaders, getInActiveLeaders, getAllDisApprovalLeaders, getAllDetails, pendingToApproved, approvedToBlocked, pendingToDisApproval, blockedToApproved, getAllOnlineLeaders, getCarsCount, getDriverSearchingCars } = require('../controllers/AuthAdmin');

// Driver Controller API's
const { findDriverByLocation, updateDriverLocation, findDriverByRadius, findDriverByStatus, ridesHistory, countDriverByRadius, findDriverId, driverFiles, saveDriverOrder, updateOrderStatus, showorderStatus, rideWeekHistory, test, saveReviews, showReviews, updateOrderStatusCus,
    driverAcceptRide,
    driverRejectRide,
    driverCancelRide,
    customerCancelRide,
    getVehicleDetail,
    driverInfoBasic,
    startRide,
    driverArrivals,
    getRideStatus,
    getDriverDeviceToken,
    cancelRide,
    statusOnDriver,
    updateDriverRating,
    calculateLoginTime, cancelOrder,
    inviteContactsSearchDriver, driverWallet, getDriverWallet, getCitiesAirports, getAlldriverWallet, getServerTime
    , getDriverTotalRidesAndTime,
    getAllDriversOrCustomerContacts,
    testPusher, saveDriverPreferences,
    showDriverPreferences, sendEmailForHelp
    , sendInvitationThroughSms } = require('../controllers/driverController');

const { HelpQuestionsAndAnswer, addNewQuestion, showAllHelpQuestions, showChatbotHistory } = require('../controllers/HelpQuestionsController');
const { saveNewNotification, getLeaderNotification } = require('../controllers/notificationController');
const { chatBotForCustomer } = require('../controllers/customerChatbot');
const auth = require('../../authentication');
const { showPasses } = require('../controllers/passes');
var router = require('express').Router();
router.get('/showAllHelpQuestions', showAllHelpQuestions);
router.post('/addNewQuestion', addNewQuestion);
router.post('/customerChatBot', chatBotForCustomer);
router.post('/getLeaderNotification', getLeaderNotification);
router.post('/saveNewNotification', saveNewNotification)
router.use(require('../routes/payoutMethods'))
router.use(require('../routes/rating'));
router.post('/getChatbotHistory', showChatbotHistory);
router.post('/helpChatBot', HelpQuestionsAndAnswer);
router.post('/sendEmailForHelp', sendEmailForHelp);
router.post('/sendInvitationThroughSms', sendInvitationThroughSms);
router.post('/showDriverPreferences', showDriverPreferences);
router.post('/saveDriverPreferences', saveDriverPreferences);
router.post('/testPusher', testPusher);
router.post('/getAllDriversOrCustomerContacts', getAllDriversOrCustomerContacts);
router.post('/getDriverTotalRidesAndTime', getDriverTotalRidesAndTime);
router.get('/getServerTime', getServerTime);
router.post('/getAllPasses', showPasses);
router.get('/getAlldriverWallets/:page/:limit', getAlldriverWallet);
router.post('/getCityAirports', getCitiesAirports);
router.post('/getDriverWallet', getDriverWallet)
router.post('/driverWallet', driverWallet);
router.post('/inviteContactsSearchForDriver', inviteContactsSearchDriver)
router.post('/cancelDriverOrder', cancelOrder);
router.post('/calculateLoginTime', calculateLoginTime);
router.post('/updateDriverRating', updateDriverRating)
router.post('/updateDriverStatus', statusOnDriver)
router.post('/cancelRide', cancelRide);
router.post('/getDriverDeviceToken', getDriverDeviceToken)
router.post('/getRideStatus', getRideStatus);
router.post('/driverArrival', driverArrivals);
router.post('/findDriverProfileById', driverInfoBasic)
router.post('/startRide', startRide);
router.post('/getVehicleDetail', getVehicleDetail);
router.post('/customerCancelRide', customerCancelRide);
router.post('/driverCancelRide', driverCancelRide);
router.post('/driverRejectRide', driverRejectRide);
router.post('/driverAcceptRide', driverAcceptRide);
router.post('/rejectRideOrderForDriver', updateOrderStatusCus);
router.post('/showReviews', showReviews)
router.post('/saveReviews', saveReviews)
router.post('/test', test);
router.post('/rideWeekHistory', rideWeekHistory);
router.post('/showorderStatus', showorderStatus);
router.post('/updateRideStatus', updateOrderStatus);
router.post('/saveDriverOrder', saveDriverOrder)
router.post('/driverDocuments', driverFiles)
router.post('/rideHistory', ridesHistory);
router.post('/findDriverIdByRadius', findDriverId);
router.post('/countDriverByRadius', countDriverByRadius)
router.post('/findDriverByLocation', findDriverByLocation);
router.post('/updateDriverLocation', updateDriverLocation);
router.post('/findDriverByRadius', findDriverByRadius);
router.post('/findDriverByStatus', findDriverByStatus);
// AuthN Admin Leaders API's
router.post('/getDriverSearchingCars', getDriverSearchingCars);
router.get('/getCarsCounter', getCarsCount);
router.post('/getAllOnlineLeaders/:page/:limit', getAllOnlineLeaders);
router.post('/getAllApprovedLeaders/:page/:limit', getAllApprovedLeaders);
router.post('/getAllBlockedLeaders/:page/:limit', getAllBlockedLeaders);
router.post('/getAllPendingLeaders/:page/:limit', getAllPendingLeaders);
router.post('/getAllDisApprovalLeaders/:page/:limit', getAllDisApprovalLeaders);
router.post('/getActiveLeaders/:page/:limit', getActiveLeaders);
router.post('/getInActiveLeaders/:page/:limit', getInActiveLeaders);
router.post('/getAllDetails', getAllDetails);
router.post('/pendingToApproved', pendingToApproved);
router.post('/approvedToBlocked', approvedToBlocked);
router.post('/blockedToApproved', blockedToApproved);
router.post('/pendingToDisApproval', pendingToDisApproval);
router.post('/disApprovedToPending', disApprovedToPending);
router.post('/updateDriverInfo', updateDriverInfo);
// OTP AuthN APi's
router.post('/verify', otp_verification_start);
router.post('/is_verified', otp_verification_check);
// AuthN Driver API's
router.post('/temporarySignUp', temporarySignUp);
router.post('/pendingSignUp', pendingSignUp);
router.post('/verifyPinCode', verifyPinCode);
router.post('/touchIdLogin', touchIdLogin);
router.post('/driverDetails', driverDetails);
router.post('/refreshDeviceToken', refreshDeviceToken)
router.post('/logout', logout)
// Drivers Location Routes
router.post('/createUpdateDriverLocation', createUpdateDriverLocation);
router.post('/findNearByDrivers', findNearByDrivers);
router.post('/changeDriverStatus', changeDriverStatus);
// For Driver Order Response Status
router.post('/getDriverOrderResponseStatus', getDriverOrderResponseStatus);
router.post('/acceptOrRejectOrder', acceptOrRejectOrder);
// VehicleCategories API's
router.post('/getAllCategoryNames', getAllCategoryNames);
router.post('/getVehiclesByCategory', getVehiclesByCategory);
router.post('/getVehicleByDriverId', getVehicleByDriverId);
router.post('/getListOfDriverVehicles', getListOfDriverVehicles);

module.exports = router;