const router = require('express').Router();
const { updatePayment, getPayoutMethods, getLeaderPayoutMethodById } = require('../controllers/driverPayoutMethod');

router.post('/addNewPayoutMethod', updatePayment);
router.post('/getPayoutMethods', getPayoutMethods);
router.post('/getLeaderPayoutMethodById', getLeaderPayoutMethodById)
module.exports = router;
