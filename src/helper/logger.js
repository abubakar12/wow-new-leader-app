var winston = require('winston');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf,prettyPrint  } = format;
const moment = require('moment')
const myFormat = printf(({ level, message, timestamp }) => {
  return `${moment(timestamp).format('dddd MMM YYYY LT')}   ${level}: ${message}`;
});
var options = {
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};
var logger = new winston.createLogger({
  format: combine(
    timestamp(),
    myFormat
  ),
  transports: [
    new winston.transports.Console(options.console)
  ],
  exitOnError: false,
});
logger.stream = {
  write: function (message, encoding) {
    logger.info(message);
  },
};

module.exports = logger;