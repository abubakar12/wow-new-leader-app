const Leader = require('../models/AuthN');
const PendingLeader = require('../models/pendingLeaders');
const BlockLeader = require('../models/blockedLeaders');
const DisApproveLeader = require('../models/disApprovalLeaders');
const VehicleCard = require('../models/AuthNVehicle');
const DriverCard = require('../models/AuthNDriver');
const VehicleCardTemp = require('../models/AuthNVehicleTemp');
const DriverCardTemp = require('../models/AuthNDriverTemp');
const driverLocations = require('../models/driverLocations');
const rideOrders = require('../models/rider/rideOrders');
const TwilioNumber = require('../Constants/Twilio');
const pusher = require('../util/pusher');
const moment = require('moment');
const payoutMethod = require('../models/LeaderPayoutMethods');
const { decodeBase64Image } = require('../util/ImageBase64');
const sgMail = require("@sendgrid/mail");
const AllCrediential = require('../config/allCredential');
sgMail.setApiKey(AllCrediential.twilioSendGridApi, AllCrediential.twilioKeyId);
const client = require('twilio')(AllCrediential.accountSid, AllCrediential.authToken);
module.exports = class LeaderDAO {
    async getDisApproveReason(driverId) {
        let reason = await DisApproveLeader.findOne({ driverId }, { disApprovalReason: 1 });
        return reason;
    }
    async getBlockedReason(driverId) {
        let reason = await BlockLeader.findOne({ driverId }, { blockedReason: 1 });
        return reason;
    }
    async getPendingLeaderByFilters(countryId, cityId, startDate, endDate, page, limit) {
        // All
        if (!countryId && !cityId && !startDate && !endDate) {
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false }, { page: page, limit: limit, lean: true });
            return approvedLeaders
        }
        //country ID check
        else if (countryId && !cityId && !startDate && !endDate) {
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false, countryId: countryId }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // startDate Check
        else if (startDate && !cityId && !countryId && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // endDate Check
        else if (endDate && !countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // countryID And CityID Check
        else if (countryId && cityId && !startDate && !endDate) {
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false, countryId: countryId, cityId: cityId }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        //countryID && startDate Check
        else if (countryId && startDate && !cityId && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false, countryId: countryId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // countryID && endDate Check
        else if (endDate && countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // CountryID && StartDate && endDate Check
        else if (startDate && endDate && countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false, countryId: countryId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        //countryID && CityID && startDate Check
        else if (countryId && cityId && startDate && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false, countryId: countryId, cityId: cityId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // countryID && CityID && endDate Check
        else if (endDate && countryId && cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // startDate && endDate Check
        else if (startDate && endDate && !countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // All Possible Check
        else if (startDate && endDate && countryId && cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await PendingLeader.paginate({ disapprovedState: false, countryId: countryId, cityId: cityId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        console.log(approvedLeaders);
    }
    async getApprovedLeaderByFilters(countryId, cityId, startDate, endDate, page, limit) {
        // All
        if (!countryId && !cityId && !startDate && !endDate) {
            let approvedLeaders = await Leader.paginate({ blockedState: false }, { page: page, limit: limit, lean: true });
            return approvedLeaders
        }
        // countryID And CityID Check
        else if (countryId && cityId && !startDate && !endDate) {
            let approvedLeaders = await Leader.paginate({ blockedState: false, countryId: countryId, cityId: cityId }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // countryID Check
        else if (countryId && !cityId && !startDate && !endDate) {
            let approvedLeaders = await Leader.paginate({ blockedState: false, countryId: countryId }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // startDate Check
        else if (startDate && !cityId && !countryId && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: false, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // endDate Check
        else if (endDate && !countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await Leader.paginate({ blockedState: false, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        //countryID && startDate Check
        else if (countryId && startDate && !cityId && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: false, countryId: countryId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // countryID && endDate Check
        else if (endDate && countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await Leader.paginate({ blockedState: false, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // CountryID && StartDate && endDate Check
        else if (startDate && endDate && countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: false, countryId: countryId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        //countryID && CityID && startDate Check
        else if (countryId && cityId && startDate && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: false, countryId: countryId, cityId: cityId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // countryID && CityID && endDate Check
        else if (endDate && countryId && cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await Leader.paginate({ blockedState: false, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // startDate && endDate Check
        else if (startDate && endDate && !countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: false, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // All Possible Check
        else if (startDate && endDate && countryId && cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: false, countryId: countryId, cityId: cityId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            console.log(approvedLeaders);
            return approvedLeaders
        }
    }
    async getBlockedLeadersByFilters(countryId, cityId, startDate, endDate, page, limit) {
        ////// All
        if (!countryId && !cityId && !startDate && !endDate) {
            let approvedLeaders = await Leader.paginate({ blockedState: true, }, { page: page, limit: limit, lean: true });
            return approvedLeaders
        }
        // countryID And CityID Check
        else if (countryId && cityId && !startDate && !endDate) {
            let approvedLeaders = await Leader.paginate({ blockedState: true, countryId: countryId, cityId: cityId }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        //country id check
        else if (countryId && !cityId && !startDate && !endDate) {
            let approvedLeaders = await Leader.paginate({ blockedState: true, countryId: countryId }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // startDate Check
        else if (startDate && !cityId && !countryId && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: true, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // endDate Check
        else if (endDate && !countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await Leader.paginate({ blockedState: true, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        //countryID && startDate Check
        else if (countryId && startDate && !cityId && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: true, countryId: countryId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // countryID && endDate Check
        else if (endDate && countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await Leader.paginate({ blockedState: true, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // CountryID && StartDate && endDate Check
        else if (startDate && endDate && countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: true, countryId: countryId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        //countryID && CityID && startDate Check
        else if (countryId && cityId && startDate && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: true, countryId: countryId, cityId: cityId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // countryID && CityID && endDate Check
        else if (endDate && countryId && cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await Leader.paginate({ blockedState: true, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // startDate && endDate Check
        else if (startDate && endDate && !countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: true, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // All Possible Check
        else if (startDate && endDate && countryId && cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ blockedState: true, countryId: countryId, cityId: cityId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            console.log(approvedLeaders);
            return approvedLeaders
        }
    }
    async getDisApprovedLeadersByFilters(countryId, cityId, startDate, endDate, page, limit) {
        ////// All
        if (!countryId && !cityId && !startDate && !endDate) {
            let disApprovedLeaders = await PendingLeader.paginate({ disapprovedState: true, }, { page: page, limit: limit, lean: true });
            return disApprovedLeaders
        }
        // countryID And CityID Check
        else if (countryId && cityId && !startDate && !endDate) {
            let disApprovedLeaders = await PendingLeader.paginate({ disapprovedState: true, countryId: countryId, cityId: cityId }, { lean: true, page: page, limit: limit });
            return disApprovedLeaders
        }
        // countryID Check
        else if (countryId && !cityId && !startDate && !endDate) {
            let disApprovedLeaders = await PendingLeader.paginate({ disapprovedState: true, countryId: countryId }, { lean: true, page: page, limit: limit });
            return disApprovedLeaders
        }
        // startDate Check
        else if (startDate && !cityId && !countryId && !endDate) {
            let start = new Date(startDate).toISOString();
            let disApprovedLeaders = await PendingLeader.paginate({ disapprovedState: true, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return disApprovedLeaders
        }
        // endDate Check
        else if (endDate && !countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let disApprovedLeaders = await PendingLeader.paginate({ disapprovedState: true, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return disApprovedLeaders
        }
        //countryID && startDate Check
        else if (countryId && startDate && !cityId && !endDate) {
            let start = new Date(startDate).toISOString();
            let disApprovalLeaders = await PendingLeader.paginate({ disapprovedState: true, countryId: countryId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return disApprovalLeaders
        }
        // countryID && endDate Check
        else if (endDate && countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let disApprovalLeaders = await PendingLeader.paginate({ disapprovedState: true, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return disApprovalLeaders
        }
        // CountryID && StartDate && endDate Check
        else if (startDate && endDate && countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let disApprovalLeaders = await PendingLeader.paginate({ disapprovedState: true, countryId: countryId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return disApprovalLeaders
        }
        //countryID && CityID && startDate Check
        else if (countryId && cityId && startDate && !endDate) {
            let start = new Date(startDate).toISOString();
            let disApprovedLeaders = await PendingLeader.paginate({ disapprovedState: true, countryId: countryId, cityId: cityId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return disApprovedLeaders
        }
        // countryID && CityID && endDate Check
        else if (endDate && countryId && cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let disApprovedLeaders = await PendingLeader.paginate({ disapprovedState: true, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return disApprovedLeaders
        }
        // startDate && endDate Check
        else if (startDate && endDate && !countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let disApprovedLeaders = await PendingLeader.paginate({ disapprovedState: true, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return disApprovedLeaders
        }
        // All Possible Check
        else if (startDate && endDate && countryId && cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let disApprovedLeaders = await PendingLeader.paginate({ disapprovedState: true, countryId: countryId, cityId: cityId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            console.log(disApprovedLeaders);
            return disApprovedLeaders
        }
    }
    async getActiveLeadersByFilters(countryId, cityId, startDate, endDate, activeLeaders, page, limit) {
        // All
        if (!countryId && !cityId && !startDate && !endDate) {
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders } }, { page: page, limit: limit, lean: true });
            return ActiveLeaders
        }
        // countryID And CityID Check
        else if (countryId && cityId && !startDate && !endDate) {
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders }, countryId: countryId, cityId: cityId }, { lean: true, page: page, limit: limit });
            return ActiveLeaders
        }
        // countryID Check
        else if (countryId && !cityId && !startDate && !endDate) {
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders }, countryId: countryId }, { lean: true, page: page, limit: limit });
            return ActiveLeaders
        }
        // startDate Check
        else if (startDate && !cityId && !countryId && !endDate) {
            let start = new Date(startDate).toISOString();
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders }, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return ActiveLeaders
        }
        // endDate Check
        else if (endDate && !countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders }, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return ActiveLeaders
        }
        //countryID && startDate Check
        else if (countryId && startDate && !cityId && !endDate) {
            let start = new Date(startDate).toISOString();
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders }, countryId: countryId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return ActiveLeaders
        }
        // countryID && endDate Check
        else if (endDate && countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders }, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return ActiveLeaders
        }
        // CountryID && StartDate && endDate Check
        else if (startDate && endDate && countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders }, countryId: countryId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return ActiveLeaders
        }
        //countryID && CityID && startDate Check
        else if (countryId && cityId && startDate && !endDate) {
            let start = new Date(startDate).toISOString();
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders }, countryId: countryId, cityId: cityId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return ActiveLeaders
        }
        // countryID && CityID && endDate Check
        else if (endDate && countryId && cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders }, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return ActiveLeaders
        }
        // startDate && endDate Check
        else if (startDate && endDate && !countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders }, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return ActiveLeaders
        }
        // All Possible Check
        else if (startDate && endDate && countryId && cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let ActiveLeaders = await Leader.paginate({ _id: { $in: activeLeaders }, countryId: countryId, cityId: cityId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            console.log(ActiveLeaders);
            return ActiveLeaders
        }
    }
    async getInActiveLeadersByFilters(countryId, cityId, startDate, endDate, inActiveLeaders, page, limit) {
        // All
        if (!countryId && !cityId && !startDate && !endDate) {
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders } }, { page: page, limit: limit, lean: true });
            return InActiveLeaders
        }
        // countryID And CityID Check
        else if (countryId && cityId && !startDate && !endDate) {
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, countryId: countryId, cityId: cityId }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        // countryID Check
        else if (countryId && !cityId && !startDate && !endDate) {
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, countryId: countryId }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        // startDate Check
        else if (startDate && !cityId && !countryId && !endDate) {
            let start = new Date(startDate).toISOString();
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        // countryID && startDate Check
        else if (startDate && !cityId && !countryId && !endDate) {
            let start = new Date(startDate).toISOString();
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, countryId: countryId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        // countryID && endDate Check
        else if (endDate && !countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        // endDate Check
        else if (endDate && !countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        //countryID && startDate Check
        else if (countryId && startDate && !cityId && !endDate) {
            let start = new Date(startDate).toISOString();
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, countryId: countryId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        // countryID && endDate Check
        else if (endDate && countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        // CountryID && StartDate && endDate Check
        else if (startDate && endDate && countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, countryId: countryId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        //countryID && CityID && startDate Check
        else if (countryId && cityId && startDate && !endDate) {
            let start = new Date(startDate).toISOString();
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, countryId: countryId, cityId: cityId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        // countryID && CityID && endDate Check
        else if (endDate && countryId && cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        // startDate && endDate Check
        else if (startDate && endDate && !countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return InActiveLeaders
        }
        // All Possible Check
        else if (startDate && endDate && countryId && cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let InActiveLeaders = await Leader.paginate({ _id: { $in: inActiveLeaders }, countryId: countryId, cityId: cityId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            console.log(InActiveLeaders);
            return InActiveLeaders
        }
    }
    async getOnlineDrivers(countryId, cityId, startDate, endDate, page, limit) {
        // All
        if (!countryId && !cityId && !startDate && !endDate) {
            let approvedLeaders = await Leader.paginate({ loggedIn: true }, { page: page, limit: limit, lean: true });
            return approvedLeaders
        }
        // countryID And CityID Check
        else if (countryId && cityId && !startDate && !endDate) {
            let approvedLeaders = await Leader.paginate({ loggedIn: true, countryId: countryId, cityId: cityId }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        //country ID check
        else if (countryId && !cityId && !startDate && !endDate) {
            let approvedLeaders = await Leader.paginate({ countryId: countryId }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // startDate Check
        else if (startDate && !cityId && !countryId && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ loggedIn: true, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // endDate Check
        else if (endDate && !countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await Leader.paginate({ loggedIn: true, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        //countryID && startDate Check
        else if (countryId && startDate && !cityId && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ loggedIn: true, countryId: countryId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // countryID && endDate Check
        else if (endDate && countryId && !cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await Leader.paginate({ loggedIn: true, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // CountryID && StartDate && endDate Check
        else if (startDate && endDate && countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ loggedIn: true, countryId: countryId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        //countryID && CityID && startDate Check
        else if (countryId && cityId && startDate && !endDate) {
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ loggedIn: true, countryId: countryId, cityId: cityId, createdAt: { $gte: start } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // countryID && CityID && endDate Check
        else if (endDate && countryId && cityId && !startDate) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let approvedLeaders = await Leader.paginate({ loggedIn: true, countryId: countryId, createdAt: { $lte: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // startDate && endDate Check
        else if (startDate && endDate && !countryId && !cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ loggedIn: true, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            return approvedLeaders
        }
        // All Possible Check
        else if (startDate && endDate && countryId && cityId) {
            let end = new Date(endDate);
            end.setDate(end.getDate() + 1);
            let start = new Date(startDate).toISOString();
            let approvedLeaders = await Leader.paginate({ loggedIn: true, countryId: countryId, cityId: cityId, createdAt: { $gte: start, $lt: end.toISOString() } }, { lean: true, page: page, limit: limit });
            console.log(approvedLeaders);
            return approvedLeaders
        }
    }
    async getAllLeaderDetailsFromTables(driverId) {
        const leaderDetails = await Leader.findOne({ _id: driverId })
        if (!leaderDetails) {
            const leaderDetails = await PendingLeader.findOne({ _id: driverId })
            const vehicleDetails = await VehicleCardTemp.findOne({ driverId: leaderDetails._id })
            const driverDetails = await DriverCardTemp.findOne({ driverId: leaderDetails._id })
            return { leaderDetails, vehicleDetails, driverDetails };
        }
        const vehicleDetails = await VehicleCard.findOne({ driverId: leaderDetails._id })
        const driverDetails = await DriverCard.findOne({ driverId: leaderDetails._id })
        return { leaderDetails, vehicleDetails, driverDetails };
    }
    async approveToBlock(driverId, blockedReason) {
        const leaders = await Leader.findOneAndUpdate({ _id: driverId }, { blockedState: true, blockedTime: new Date() });
        const block = new BlockLeader({ driverId: leaders._id, blockedReason: blockedReason });
        const emailMsg = {
            to: `${leaders.email}`, from: "wowetsinc@gmail.com", subject: "Violation Alert", text: `Your Pin ${leaders.pinCode}`,
            html: `<strong><h4>Dear ${leaders.first_name} ${leaders.last_name}</h4></strong>
				<br>It’s been written to apprise that you have violated the terms & conditions and privacy policy of WOW. Tragically, your account has been blocked by WOW Security Team. For further info, please contact WOW Helpline and Security Support.</br>
                <br>For further info contact info@wowets.com</br>
				<br>WOW Helpline +971 45 217500</br>
				<br>Regards,</br>
                <br>WOW ETS.</br>`,
        }
        const phoneMsg = {
            to: `${leaders.fullPhoneNumber}`, from: TwilioNumber.TWILIO_NUMBER,
            body: `Dear ${leaders.first_name}${leaders.last_name},You have violated the terms & conditions and privacy policy of WOW ETS.Unfortunately, your account has been blocked by WOW Security Team. For further info, please contact WOW Helpline and support.`
        }
        await block.save();
        sgMail.send(emailMsg);
        console.log(emailMsg);
        client.messages.create(phoneMsg).then((message) => console.log(message.to))
        return { msg: 'Leader BLOCKED' };
    }
    async blockToApprove(driverId) {
        const reduction = await BlockLeader.findOneAndRemove({ driverId })
        await Leader.findOneAndUpdate({ _id: reduction.driverId }, { blockedState: false, approveTime: new Date() })
        return { msg: 'Leader Went To APPROVED' };
    }
    async pendingToDisApprove(driverId, disApprovalReason) {
        const pendingleaders = await PendingLeader.findOneAndUpdate({ _id: driverId }, { disapprovedState: true }, { new: true })
        const appr = new DisApproveLeader({ driverId: pendingleaders._id, disApprovalReason: disApprovalReason });
        const emailMsg = {
            to: `${pendingleaders.email}`,
            from: "wowetsinc@gmail.com",
            subject: "DisApproved Notification",
            text: `Your Pin ${pendingleaders.pinCode}`,
            html: `<strong><h4>Dear ${pendingleaders.first_name} ${pendingleaders.last_name}</h4></strong>
				<br>Thank You for showing your interest in becoming a Leader of WOW Electronics Transport Services.</br>
				<br>This is to notify you that your application was received and reviewed. Unfortunately, your profile and document doesn’t meet the criteria of WOW. Sadly, your account has not been approved by WOW Security Team. For further info, please contact WOW Helpline and support.</br>
                <br>For any queries contact info@wowets.com</br>
                <br>WOW Helpline +971 45 217500</br>
                <br>Regards,</br>
                <br>WOW ETS Team.</br>`,
        }
        const phoneMsg = {
            to: `${pendingleaders.fullPhoneNumber}`,
            from: TwilioNumber.TWILIO_NUMBER,
            body: `Dear ${pendingleaders.first_name} ${pendingleaders.last_name},your application was received and reviewed.Unfortunately, your profile and document doesn’t meet the criteria of WOW. So your account has been disapproved by WOW Security Team. For further info, please contact WOW Helpline and support for further updating of profile and documents.`
        }
        await appr.save();
        sgMail.send(emailMsg);
        client.messages.create(phoneMsg).then((message) => console.log(message.to))
        return { msg: 'Leader DisApproved' };
    }
    async disApproveToPending(driverId) {
        const reduction = await DisApproveLeader.findOneAndRemove({ driverId: driverId })
        await PendingLeader.findByIdAndUpdate({ _id: reduction.driverId }, { disapprovedState: false }, { new: true })
        return { msg: 'Leader Went To Pending' };
    }
    async pendingToApprove(driverId) {
        let leaders = await PendingLeader.findByIdAndRemove({ _id: driverId })
        const appr = new Leader({ first_name: leaders.first_name, last_name: leaders.last_name, email: leaders.email, countryCode: leaders.countryCode, phoneNumber: leaders.phoneNumber, fullPhoneNumber: leaders.fullPhoneNumber, gender: leaders.gender, pinCode: leaders.pinCode, emiratesID: leaders.emiratesID, profileImage: leaders.profileImage, nationality: leaders.nationality, licenseNumber: leaders.licenseNumber, countryId: leaders.countryId, cityId: leaders.cityId });
        appr.approveTime = new Date();
        const driver = await appr.save();
        console.log(driver)
        const emailMsg = {
            to: `${leaders.email}`,
            from: "wowetsinc@gmail.com",
            subject: "WELCOME TO WOW",
            text: `Your Pin ${leaders.pinCode}`,
            html: `<strong><h4>Dear ${leaders.first_name} ${leaders.last_name}</h4></strong>
				<br>Thank You for showing your interest in becoming a Leader of WOW Electronics Transport Services.</br>
				<br>Your profile application has been received and reviewed. Your documents are completed according to WOW requirements. So you have successfully rolled in WOW Leader App.</br>
                <br>Your credentials for logging into WOW App are as follows:</br>
                <br>PINCODE: ${leaders.pinCode}</br>
                <br>We wish you the best of luck in your endeavors.</br>
                <br>Thank You for registering with WOW.</br>
                <br>If you have any queries regarding with sign up feel free to contact at the provided e-mail address: info@wowets.com</br>
                <br>Regards,</br>
                <br>WOW ETS Team.</br>`,
        }
        const phoneMsg = {
            to: `${leaders.fullPhoneNumber}`, from: TwilioNumber.TWILIO_NUMBER,
            body: `Dear ${leaders.first_name} ${leaders.last_name},after reviewing your profile and documents, we would like to inform you that your account has been successfully approved by WOW security team. Your PINCODE is ${leaders.pinCode}. This PINCODE is private and confidential so we request you to never share this SMS with anyone. Thank You for showing interest and registering with WOW.`
        }
        const data = await VehicleCardTemp.findOneAndRemove({ driverId: leaders._id })
        const vcards = new VehicleCard({ driverId: driverId, categoryId: data.categoryId, categoryName: data.categoryName, vehicleId: data.vehicleId, vehicleName: data.vehicleName, registrationNo: data.registrationNo, year: data.year, color: data.color, vehicle_reg_f: data.vehicle_reg_f, expiryDate: data.expiryDate, vehicle_reg_b: data.vehicle_reg_b });
        await vcards.save();
        const ddata = await DriverCardTemp.findOneAndRemove({ driverId: leaders._id })
        const dcards = new DriverCard({ driverId: driverId, driver_photo: ddata.driver_photo, emirates_card_f: ddata.emirates_card_f, expiry_date_emirates: ddata.expiry_date_emirates, emirates_card_b: ddata.emirates_card_b, rta_card_f: ddata.rta_card_f, expiry_date_rta: ddata.expiry_date_rta, rta_card_b: ddata.rta_card_b });
        await dcards.save();
        await DriverCard.findOneAndUpdate({ driverId: leaders._id }, { driverId: driver._id }, { new: true });
        await VehicleCard.findOneAndUpdate({ driverId: leaders._id }, { driverId: driver._id }, { new: true });
        await payoutMethod.findOneAndUpdate({driverId:driverId},{driverId: driver._id,status:'approve'},{new : true});
        sgMail.send(emailMsg);
        client.messages.create(phoneMsg).then((message) => console.log(message.to))
        return { msg: 'Approved Sucessfully', approvedId: driver._id };
    }
    async driverRegistrationByAdmin() {
        let ImgVF = decodeBase64Image(req.body.vehicle_reg_f);
        let ImgVB = decodeBase64Image(req.body.vehicle_reg_b);
        let ImgDrvP = decodeBase64Image(req.body.driver_photo);
        let ImgEmirF = decodeBase64Image(req.body.emirates_card_f);
        let ImgEmirB = decodeBase64Image(req.body.emirates_card_b);
        let ImgRtaF = decodeBase64Image(req.body.rta_card_f);
        let ImgRtaB = decodeBase64Image(req.body.rta_card_b);
        const [vF, vB, drvImg, emiF, emiB, rtF, rtB] = await Promise.all([ImgVF, ImgVB, ImgDrvP, ImgEmirF, ImgEmirB, ImgRtaF, ImgRtaB]);
        const addLeader = new pendingLeaders(req.body);
        const leaderData = await addLeader.save();
        if (!leaderData) { return res.json({ code: '400', response: { msg: 'An error occured' } }) }
        const driverId = leaderData._id;
        const vKeyF = 'vehiclesDocs/' + driverId + '_vehfront.jpg';
        const vKeyB = 'vehiclesDocs/' + driverId + '_vehback.jpg';
        const vFront = cloudBase.upload({ Bucket: bucket, ACL: acl, ContentEncoding: contentEncoding, Body: vF.data, Key: vKeyF }).promise();
        const vBack = cloudBase.upload({ Bucket: bucket, ACL: acl, Body: vB.data, ContentEncoding: contentEncoding, Key: vKeyB }).promise();
        const [vehF, vehB] = await Promise.all([vFront, vBack]);
        let vcards = new vehiclefiles_temp(req.body);
        vcards.driverId = driverId,
            vcards.vehicle_reg_f = vehF.Location,
            vcards.vehicle_reg_b = vehB.Location
        const vehicleData = await vcards.save();
        if (!vehicleData) { return res.json({ code: '400', response: { msg: 'Vehicle error' } }) }
        const DrvKey = 'profileImg/' + driverId + '_drv_Photo.jpg';
        const EKeyF = 'driversDocs/' + driverId + '_efront.jpg';
        const EkeyB = 'driversDocs/' + driverId + '_eback.jpg';
        const RKeyF = 'driversDocs/' + driverId + '_rtafront.jpg';
        const RKeyB = 'driversDocs/' + driverId + '_rtaback.jpg';
        const drvP = cloudBase.upload({ Bucket: bucket, ACL: acl, ContentEncoding: contentEncoding, Body: drvImg.data, Key: DrvKey }).promise();
        const emirFront = cloudBase.upload({ Bucket: bucket, ACL: acl, ContentEncoding: contentEncoding, Body: emiF.data, Key: EKeyF }).promise();
        const emirBack = cloudBase.upload({ Bucket: bucket, ACL: acl, ContentEncoding: contentEncoding, Body: emiB.data, Key: EkeyB }).promise();
        const rtaFront = cloudBase.upload({ Bucket: bucket, ACL: acl, ContentEncoding: contentEncoding, Body: rtF.data, Key: RKeyF }).promise();
        const rtaBack = cloudBase.upload({ Bucket: bucket, ACL: acl, ContentEncoding: contentEncoding, Body: rtB.data, Key: RKeyB }).promise();
        const [drvPhoto, emirF, emirB, rtaF, rtaB] = await Promise.all([drvP, emirFront, emirBack, rtaFront, rtaBack]);
        const dcards = new driverfiles_temp({ driverId: driverId, driver_photo: drvPhoto.Location, emirates_card_f: emirF.Location, emirates_card_b: emirB.Location, rta_card_f: rtaF.Location, rta_card_b: rtaB.Location, expiry_date_emirates: req.body.expiry_date_emirates, expiry_date_rta: req.body.expiry_date_rta });
        const drvData = await dcards.save();
        if (!drvData) { return res.json({ code: '400', response: { msg: 'An error Occured' } }) }
        const driversignup = await pendingLeaders.findOneAndUpdate({ _id: drvData.driverId }, { profileImage: drvData.driver_photo }, { new: true });
        if (!driversignup) { return res.json({ code: '400', response: { msg: 'vehicle temp id data not found' } }) }
        return res.json({ code: '200', response: { msg: 'Sucessfully Add Leader', driverId: driversignup._id } })
    }
    async getCarsCountForAdminMap() {
        let totalCars = await driverLocations.find({}).countDocuments();
        driverLocations.watch().on('change', async data => {
            let allCars = await driverLocations.find({ updatedAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } }, { location: 1, updatedAt: 1 });
            pusher.trigger('my-new-event', 'allCars', { allCars, numbers: allCars.length });

        });
        let offlineCars = await driverLocations.find({ status: 'off' }).countDocuments();
        driverLocations.watch().on('change', async data => {
            let allOfflineCars = await driverLocations.find({ updatedAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } }, { location: 1, updatedAt: 1 });
            pusher.trigger('my-new-event', 'allOfflineCars', { allOfflineCars, numbers: allOfflineCars.length });

        })
        let orderAccepted = await rideOrders.find({ status: 'engaged' }).countDocuments();
        rideOrders.watch().on('change', async data => {
            let allOrderAccepted = await rideOrders.find({ updatedAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } }, { location: 1, updatedAt: 1 });
            pusher.trigger('my-new-event', 'allOrderAccepted', { allOrderAccepted, numbers: allOrderAccepted.length });

        })
        let driverSearching = await driverLocations.find({ status: 'busy' }).countDocuments();
        driverLocations.watch().on('change', async data => {
            let allDriverSearching = await driverLocations.find({ updatedAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } }, { location: 1, updatedAt: 1 });
            pusher.trigger('my-new-event', 'allDriverSearching', { allDriverSearching, numbers: allDriverSearching.length });
            console.log(allDriverSearching);
        })
        let waiting = await driverLocations.find({ status: 'on' }).countDocuments();
        driverLocations.watch().on('change', async data => {
            let allWaiting = await driverLocations.find({ updatedAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } }, { location: 1, updatedAt: 1 });
            pusher.trigger('my-new-event', 'allWaiting', { allWaiting, numbers: allWaiting.length });

        })
        let onJob = await rideOrders.find({ rideStatus: 'rideStarted' }).countDocuments();
        rideOrders.watch().on('change', async data => {
            let allOnJob = await rideOrders.find({ updatedAt: { $gt: moment(new Date()).startOf('D'), $lt: moment(new Date()).endOf('D') } }, { location: 1, updatedAt: 1 });
            pusher.trigger('my-new-event', 'allOnJob', { allOnJob, numbers: allOnJob.length });

        })
        return { totalCars: totalCars, offlineCars: offlineCars, orderAccepted: orderAccepted, driverSearching: driverSearching, waiting: waiting, onJob: onJob }
    }
 
     
}