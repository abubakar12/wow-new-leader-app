const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const routes = require("./src/routes");
const winston = require('./src/helper/logger');
const checkOnline = require('./CronJobs/checkOnLeaderCron');
const mapAdmin=require('./CronJobs/MapAdminCronJob');
const mapAdminSearch=require('./CronJobs/driverSearchingCars');
const app = express();
const PORT = 4000;
//Cron Jobs Running
checkOnline()
mapAdmin()
mapAdminSearch();

// Database Connection
require("./src/config/driverDBConnect");
app.use((req,res,next)=>{
    winston.info(` ${req.originalUrl} - ${req.method} - ${req.ip} `);
    next();
})
// Cors Setup
app.use(cors());
app.use(bodyParser.json({ limit: "50mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

//Static Storage
app.use(express.static("storage"));
//File Uploading
const fileUpload = require("express-fileupload");
app.use(fileUpload({ safeFileNames: true, preserveExtension: true }));

// Routers
app.use("/api/v1/driver", routes);
app.use((error,req,res,next)=>{
    winston.error(`${error.statusCode || 500}  - ${req.originalUrl} - ${req.method} - ${error.message} `);
    res.status(error.statusCode?error.statusCode : 500).json({code:'400',response:{msg:error.message}})
    next();
});


// Get Message
app.get("/", async (req, res) => { res.send(`Welcome to WOW App running on Port ${PORT}`) });
// Listening Port
app.listen(PORT, () => winston.info("Server is running Ctrl+Click to follow link on http://localhost:4000/"));
