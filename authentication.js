const jwt = require("jsonwebtoken");
const config = require("./src/config/roles");
const createServiceToken = () => {
  return jwt.sign({ role: config.roles.SERVICE }, config.privateKey, {
    expiresIn: 3600,
    algorithm: "RS256"
  });
};

const parseToken = authorizationHeader => {
  if (authorizationHeader) {
    const [bearer, token] = authorizationHeader.split(" ");
    return token;
  }
  return undefined;
};

const verifyService = (req, res, next) => {
  const token = parseToken(req.headers["authorization"]);
  if (!token) {
    const err = new Error("You are not authenticated to use this service");
    err.statusCode = 401;
    next(err);
    return;
  }
  jwt.verify(
    token,
    config.publicKey,
    { algorithm: ["RS256"] },
    (err, payload) => {
      if (err) {
        if (err.name === "TokenExpiredError") {
          err.statusCode = 406;
          next(err);
        } else {
          err.statusCode = 401;
          next(err);
          return;
        }
      }
      if (payload.role === config.roles.SERVICE) {
        next();
      } else {
        const err = new Error("You are not allowed to use this service");
        err.statusCode = 403;
        next(err);
        return;
      }
    }
  );
};

const verifyDriver = (req, res, next) => {
  const token = parseToken(req.headers["authorization"]);
  if (!token) {
    const err = new Error("You are not authenticated to use this service");
    err.statusCode = 401;
    next(err);
    return;
  }
  jwt.verify(
    token,
    config.publicKey,
    { algorithm: ["RS256"] },
    (err, payload) => {
      if (err.name === "TokenExpiredError") {
        err.statusCode = 406;
        next(err);
      } else {
        err.statusCode = 401;
        next(err);
        return;
      }
      if (
        payload.role === config.roles.DRIVER ||
        payload.role === config.roles.SERVICE
      ) {
        next();
      } else {
        const err = new Error("You are not allowed to use this service");
        err.statusCode = 403;
        next(err);
        return;
      }
    }
  );
};

const verifyCustomer = (req, res, next) => {
  const token = parseToken(req.headers["authorization"]);
  if (!token) {
    const err = new Error("You are not authenticated to use this service");
    err.statusCode = 401;
    next(err);
    return;
  }
  jwt.verify(
    token,
    config.publicKey,
    { algorithm: ["RS256"] },
    (err, payload) => {
      if (err) {
        if (err.name === "TokenExpiredError") {
          err.statusCode = 406;
          next(err);
        } else {
          err.statusCode = 401;
          next(err);
          return;
        }
      }
      if (
        payload.role === config.roles.DRIVER ||
        payload.role === config.roles.SERVICE ||
        payload.role === config.roles.CUSTOMER
      ) {
        next();
      } else {
        const err = new Error("You are not allowed to use this service");
        err.statusCode = 403;
        next(err);
        return;
      }
    }
  );
};

module.exports = {
  verifyService,
  verifyDriver,
  createServiceToken,
  verifyCustomer
};
